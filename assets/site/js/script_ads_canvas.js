var circle;
var canvas;
var elementName;
var circleX = 200;
var circleY = 200;
var circleRedious = 10;
var circleFill = '#0099DB';
var circleThick = 4;
var lineFill = 'rgba(148,148,148,0.5)';
var lineThick = 2;

var ctx;
var WIDTH;
var HEIGHT;
var INTERVAL = 20;

var isDrag = false;
var mx, my; 

var canvasValid = false;

var mySel; 

var mySelColor = '#CC0000';
var mySelWidth = 2;

var ghostcanvas;
var gctx; 

var offsetx, offsety;

var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;

function Circle(){
	this.x = 0;
	this.y = 0;
	this.rad = 10;
	this.start = 0;
	this.pi = 2*Math.PI;
	this.fill = '#444444';
}

function addCircle(x, y, rad, fill){
	circle = new Circle;
	circle.x = x;
	circle.y = y;
	circle.rad = rad;
	circle.fill = fill;
	invalidate();
}

function init() {
  elementName = 'canvas';
  canvas = document.getElementById(elementName);
  HEIGHT = canvas.height;
  WIDTH = canvas.width;
  ctx = canvas.getContext('2d');
  
  clear(ctx);
  
  ghostcanvas = document.createElement(elementName);
  ghostcanvas.height = HEIGHT;
  ghostcanvas.width = WIDTH;
  gctx = ghostcanvas.getContext('2d');

  canvas.onselectstart = function () { return false; }
  
  if (document.defaultView && document.defaultView.getComputedStyle) {
    stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
    stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
    styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
    styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  }
  
  setInterval(draw, INTERVAL);

  canvas.onmousedown = myDown;
  canvas.onmouseup = myUp;
  canvas.ondblclick = myDblClick;
 
  circleY = document.getElementById('hdn_contents_ads_v').value;
  
  circleX = WIDTH/2;
  addCircle(circleX , circleY , circleRedious , circleFill);
}



function clear(c) {
  c.clearRect(0, 0, WIDTH, HEIGHT);
}

function draw() {
  if (canvasValid == false) {
    clear(ctx);
	
	ctx.fillStyle = lineFill;
	ctx.fillRect(0,circle.y,WIDTH,lineThick);
	
	drawCircle(ctx , circle , circle.fill);
	
	drawErrorBars();
    if (mySel != null) {
		drawRuler();
    }
	
    
    canvasValid = true;
  }
}

function drawCircle(context, shape, fill) {

  if (shape.x > WIDTH || shape.y > HEIGHT) return; 
 
  context.beginPath();
  context.arc(shape.x,shape.y,shape.rad,shape.start,shape.pi);
  context.fillStyle = fill;
  context.lineWidth = lineThick;
  context.fill();
  context.strokeStyle = lineFill;
  context.stroke();
}

function drawRuler(){
	
	//Ver |
	//    |
	//    V
	ctx.fillStyle = 'rgba(158,209,255,0.5)';
	ctx.fillRect(WIDTH - 20,0,20,HEIGHT);
	
	for(var i = 0;i < HEIGHT ;){
		ctx.fillStyle = '#272727';
		ctx.fillRect(WIDTH - 20,i,20,1);
		i = i + 10;
	}
	
	
}

function drawErrorBars(){
	
	//check upper
	if(circle.y < 30 ){
		ctx.fillStyle = 'rgba(255,0,0,0.5)';
		ctx.fillRect(0,0,WIDTH,30);
	}
	//check lower
	if(circle.y > HEIGHT - 30 ){
		ctx.fillStyle = 'rgba(255,0,0,0.5)';
		ctx.fillRect(0,HEIGHT - 30,WIDTH,30);
	}
	
}

function myMove(e){
  if (isDrag){
    getMouse(e);
    
    mySel.x = WIDTH/2;
    mySel.y = my - offsety;   
	
	document.getElementById('hdn_contents_ads_v').value = mySel.y;
	validateContentsChanges();
	
	document.getElementById(elementName).style.cursor = "move";
	
	invalidate();
  }
}

function validateContentsChanges(){
	
	var c = document.getElementById('hdn_contents_ads_correct').value;
	var value = Math.round((mySel.y/HEIGHT)*10);
	document.getElementById('lbl_ads').innerHTML = "Content : " +value+" Ads : "+(10- value);
	//check upper
	if(mySel.y < 30 ){
		document.getElementById('hdn_contents_ads_correct').value = false;
	}
	//check lower
	else if(mySel.y > HEIGHT - 30 ){
		document.getElementById('hdn_contents_ads_correct').value = false;
	}
	else{
		document.getElementById('hdn_contents_ads_correct').value = true;
	}
}

function myDown(e){
  getMouse(e);
  clear(gctx);
  
  gctx.fillStyle= 'black' ;
  gctx.fillRect(circle.x - circleRedious,circle.y - circleRedious,circleRedious*2,circleRedious*2);
	
    var imageData = gctx.getImageData(mx, my, 1, 1);
    var index = (mx + my * imageData.width) * 4;
    
    if (imageData.data[3] > 0) {
	 
      mySel = circle;
      offsetx = mx - mySel.x;
      offsety = my - mySel.y;
      mySel.x = mx - offsetx;
      mySel.y = my - offsety;
      isDrag = true;
      canvas.onmousemove = myMove;
      invalidate();
      clear(gctx);
      return;
    }
  
 
  mySel = null;
 
  clear(gctx);
 
  invalidate();
}

function myUp(){
  isDrag = false;
  canvas.onmousemove = null;
  document.getElementById(elementName).style.cursor = "default";
}

function myDblClick(e) {
  getMouse(e);
}

function invalidate() {
  canvasValid = false;
}

function getMouse(e) {
      var element = canvas, offsetX = 0, offsetY = 0;

      if (element.offsetParent) {
        do {
          offsetX += element.offsetLeft;
          offsetY += element.offsetTop;
        } while ((element = element.offsetParent));
      }

      offsetX += stylePaddingLeft;
      offsetY += stylePaddingTop;

      offsetX += styleBorderLeft;
      offsetY += styleBorderTop;

      mx = e.pageX - offsetX;
      my = e.pageY - offsetY
}


