<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Main_Home_Controller extends CI_Controller {

		function __construct(){
	        parent::__construct();
	        $this->load->model('User_Model');
    	}

		public function index()
		{
				
				$this->load->view('templates/header');
				$this->load->view('main_index');
				$this->load->view('templates/popup');
				$this->load->view('templates/footer');
				
				
			
			
			
		}
		
		public function login()
		{
			
			$this->load->view('new_login');
			
			
		}
		
		public function loginUser()
		{
			
			$userid = $this->input->post('userid');
			$password = $this->input->post('password');
			$default_page=null;
			
				$user_data = $this->User_Model->checkUser(trim($userid),trim($password));
			
				if($user_data != -1){
						
					$user_session = array(
									'user_id' => $user_data['user_id'],
									'user_name' => $user_data['user_name'],
									'role_id' => $user_data['role_id'],
									'default_page' => $user_data['default_page'],
									
					);
					
					$this->session->set_userdata($user_session);
					$default_page=$user_data['default_page'];
										
					//load menu
					$query = $this->User_Model->getMenus($user_data['role_id']);
					$i = 0;
					$output = array();
					foreach ($query->result_array() as $row)
					{
					    $output[$i][0] = $row['page_lable'];
						$output[$i][1] = $row['link'];
						$i++;
						
					}
			
					$this->session->set_userdata('menu', $output);
					redirect($default_page,'refresh');
					
					
				}
				else{
					redirect('Main_Home_Controller/login','refresh');
					
				}
		
			}
		
		public function logout(){
			$this->session->sess_destroy();
			redirect('Main_Home_Controller', 'refresh');
		}
		
		
				
}
