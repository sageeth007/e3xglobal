<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
    class Manage_Request_Controller extends CI_Controller{
    	
		function __construct(){
	        parent::__construct();
	        $this->load->model('Admin_Model');
    	}
		
    	public function index()
		{
			$data['request'] = $this->Admin_Model->getAllRequest();
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('admin_views/manage_request',$data);
			$this->load->view('templates_sub/footer');
		}
		
		public function loadApprovalRequest($orgId){
			
			$data['approve_request'] = $this->Admin_Model->getApproveOrg($orgId);
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('admin_views/approval_request',$data);
			$this->load->view('templates_sub/footer');
		}
		
		public function rejectRequest($orgId){
			$check = $this->Admin_Model->deleteOrgRequest($orgId);
			
			if($check == 1){
				
				$this->session->set_flashdata('msg-success', 'Sucessfully submit.');
				
				redirect('admin_controllers/Manage_Request_Controller', 'refresh');
			}
			else{
				echo "<script>alert('Error reject..')</script>";
			}
		}
		
		public function approvalOrg($orgId){
			$orgName = $this->input->post('OrganizationName');
			$orgType = $this->input->post('OrganizationType');
			$orgEmail = $this->input->post('Email');
			$emailBody = $this->input->post('emailBody');
			
			//Send Email
			//
			//
			if($orgType == "Centerlized"){
				//Insert into users tables
				$dataSet = $this->Admin_Model->getApproveOrg($orgId);
				
				$orgUserData = array(
					"UserId"=>$dataSet->OrganizationName,
					"Name"=>$dataSet->OrganizationName,
					"Password"=>$dataSet->OrganizationName,
					"RoleId"=>2
				);
				$check = $this->Admin_Model->insertUser($orgUserData);
				
			}
			else{
				//Insert into users tables
				$dataSet = $this->Admin_Model->getApproveOrg($orgId);
				
				$orgUserData = array(
					"UserId"=>$dataSet->OrganizationName,
					"Name"=>$dataSet->OrganizationName,
					"Password"=>$dataSet->OrganizationName,
					"RoleId"=>4
				);
				$check = $this->Admin_Model->insertUser($orgUserData);
			}
			
			//Remove this from Request table
			$check = $this->Admin_Model->deleteOrgRequest($orgId);
			
			if($check == 1){
				
				$this->session->set_flashdata('msg-success', 'Sucessfully approve.');
				
				redirect('admin_controllers/Manage_Request_Controller', 'refresh');
			}
			else{
				echo "<script>alert('Error reject..')</script>";
			}
			
		}
    }
?>