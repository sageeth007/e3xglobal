<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Site_Settings extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('site_setting_model');
    }

	

    public function loadSiteSettinngs() {
        $data['site_setting'] = $this->site_setting_model->get_site_details();
        $this->load->view('templates_sub/header');
        $this->load->view('templates_sub/menu');
        $this->load->view('templates_sub/popup');
        $this->load->view('update_site_settings', $data);
        $this->load->view('templates_sub/footer');
    }
	
	
	
	


    public function load_site_configuration_details() {

        
        $data['site_setting'] = $this->site_setting_model->get_site_details();
        $this->load->view('templates_sub/header');
        $this->load->view('templates_sub/menu');
        $this->load->view('templates_sub/popup');
        $this->load->view('update_site_configuration', $data);
        $this->load->view('templates_sub/footer');
    }


    public function add_or_update_contact_details() {
        $site_address = $this->input->post('site_address');
        $map_address = $this->input->post('map_address');
        $contact_no = $this->input->post('contact_no');
        $fax_no = $this->input->post('fax_no');
        $email = $this->input->post('email');

        //validation rules
        $this->load->library('form_validation');
        $this->form_validation->set_rules('site_address', 'Site Address', 'required|trim');
        $this->form_validation->set_rules('map_address', 'Map Address', 'required|trim');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required|trim');
        $this->form_validation->set_rules('fax_no', 'Fax No', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

        if ($this->form_validation->run() == TRUE) {

            if ($this->site_setting_model->id_to_dbrow('site_setting', 'key', 'site_address') != FALSE) {

                $result = $this->site_setting_model->update('site_setting', 'key', 'site_address', array('value' => $site_address));
            } else {
                $result = $this->site_setting_model->insert('site_setting', array('key' => 'site_address', 'value' => $site_address));
            }

            if ($this->site_setting_model->id_to_dbrow('site_setting', 'key', 'map_address') != FALSE) {

                $result = $this->site_setting_model->update('site_setting', 'key', 'map_address', array('value' => $map_address));
            } else {
                $result = $this->site_setting_model->insert('site_setting', array('key' => 'map_address', 'value' => $map_address));
            }

            if ($this->site_setting_model->id_to_dbrow('site_setting', 'key', 'contact_no') != FALSE) {

                $result = $this->site_setting_model->update('site_setting', 'key', 'contact_no', array('value' => $contact_no));
            } else {
                $result = $this->site_setting_model->insert('site_setting', array('key' => 'contact_no', 'value' => $contact_no));
            }

            if ($this->site_setting_model->id_to_dbrow('site_setting', 'key', 'fax_no') != FALSE) {

                $result = $this->site_setting_model->update('site_setting', 'key', 'fax_no', array('value' => $fax_no));
            } else {
                $result = $this->site_setting_model->insert('site_setting', array('key' => 'fax_no', 'value' => $fax_no));
            }

            if ($this->site_setting_model->id_to_dbrow('site_setting', 'key', 'email') != FALSE) {

                $result = $this->site_setting_model->update('site_setting', 'key', 'email', array('value' => $email));
            } else {
                $result = $this->site_setting_model->insert('site_setting', array('key' => 'email', 'value' => $email));
            }
			$this->session->set_userdata(array('msg-success'=>'Sucessfully submit.'));
            
			$data['site_setting'] = $this->site_setting_model->get_site_details();
        	$this->load->view('templates_sub/header');
        	$this->load->view('templates_sub/menu');
        	$this->load->view('templates_sub/popup');
        	$this->load->view('update_site_settings', $data);
        	$this->load->view('templates_sub/footer');
			
			
        } else {
    
			$this->session->set_userdata(array('msg-error'=>'Validation error.'));
           	
            $this->load->view('templates_sub/header');
            $this->load->view('templates_sub/menu');
            $this->load->view('templates_sub/popup');
            $this->load->view('update_site_settings');
            $this->load->view('templates_sub/footer');
			
        }
    }

	

}

?>