<?php 
	if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    class App_Design_Start_Controller extends CI_Controller{
    		
		function __construct(){
	        parent::__construct();
	        $this->load->model('App_Model');
    	}
		
    	public function index()
		{
			
	
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('templates_sub/popup');
			$this->load->view('app_views/app_design_views/app_design_start');
			$this->load->view('templates_sub/footer');
			
		}
		
		
		public function saveAppDesignStart()
		{
			/*store app name and app description in a session*/
			$appName = $this->input->post('txt_appname');
			$appDescription = $this->input->post('txt_app_description');
			$nameError = "";
			$descriptionError = "";
			$error = 1;
			
			
			//check app name is allready exists
			$check = $this->App_Model->checkAppName($appName);
			if($check != 1){
				$error = -1;
			}
			
			$iconPath = $this->session->userdata('app_icon_path');
			if($this->session->userdata('app_icon_path') == ""){
				$error = -2;
			}
			
				
			if($error == 1){
				
				$appDesignStartData = array('app_name'=>$appName,'app_description'=>$appDescription);
				$this->session->set_userdata($appDesignStartData);
				$this->session->unset_userdata('app_icon_path');	
				
			}
			echo $error;
		
		}

		function doUploadAppIcon(){
			
			$imgFileName  = $_FILES['app_icon']['name'];
			$extention = strtolower(substr($imgFileName,strpos($imgFileName,'.')+1));
			$type = $_FILES['app_icon']['type'];
			
			$userId = $this->session->userdata('user_id');
			$uploadDir = "upload/";
			$appIconNew = $uploadDir . $userId . "_app_icon." . $extention;
				
			if (isset($_FILES["app_icon"])  && ($type == 'image/jpeg'|| $type == 'image/png'))
			{
			    if (($_FILES["app_icon"]["error"] > 0)) 
			    {
			        //print_r($_FILES["app_icon"]["error"]);
			        echo -1;
			    } 
			    else 
			    {
			    	$appIconPath = array('app_icon_path'=>$appIconNew);
					$this->session->set_userdata($appIconPath);
					
			        move_uploaded_file($_FILES["app_icon"]["tmp_name"], $appIconNew);
			        
			        echo "1";
			       
	    		}
			
			}
			
			else{
				echo -1;
			}
			
		}
		

	
				
    }
?>