<?php 
	if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    class App_Design_Controller extends CI_Controller{
    	
    	function __construct(){
	        parent::__construct();
	        $this->load->model('App_Model');
    	}
		
    	public function index()
		{	
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('templates_sub/popup');
			$this->load->view('app_views/app_design_views/app_home_design');
			$this->load->view('templates_sub/footer');
			
			
		}
		
		
		
		public function saveLayout(){
			
			$app_id = $this->input->post('app_id');
			$appName = $this->input->post('app_name');
			$appDescription = $this->input->post('app_desc');
			$actionBarColor = $this->input->post('action_bar_color');
			$bodyColor = $this->input->post('app_body_color');
			$contents_ads_v = $this->input->post('contents_ads_v');
			
			
			
			$userId = $this->session->userdata('user_id');
			$appLogoPath = $this->session->userdata('app_logo_path');
			
			
			
			if(!empty($appLogoPath)){
				
				$data = array(	'AppName'=>$appName,
								'AppDescription'=>trim($appDescription),
								'UserId'=>$userId,
								'ActionBarColor'=>$actionBarColor,
								'BodyColor'=>$bodyColor,
								'AppLogo'=>$appLogoPath,
								'ContentsAds_V'=>$contents_ads_v
								);
								
				if(empty($app_id)){
					//Add new app design
					$result = $this->App_Model->saveAppDesign($data);
					
					if($result){
					//	$this->session->unset_userdata('app_logo_path');
					//	$this->session->unset_userdata('app_name');
					//	$this->session->unset_userdata('app_description');
						echo 1;
					}
					else{
						echo -1;
					}
				}
				else{
					$result = $this->App_Model->updateAppDesign($app_id,$data);
					
					if($result){
					//	$this->session->unset_userdata('app_logo_path');
					//	$this->session->unset_userdata('app_name');
					//	$this->session->unset_userdata('app_description');
						
						$resultIds = $this->App_Model->getAllRegIDForUserID($userId);
						
						$data['MessageTitle'] = 'Message_1';
						
						/*Message Service Use*/
						$this->App_Model->pushMessage($userId,json_encode($data),0);
						
						/*GCM Use*/
						//$this->sendUpdateNotification($resultIds,$data);
						echo 1;
					}
					else{
						echo -1;
					}
				}
				
				
			}
			else{
				echo -2;
			}
			
		}
		
		function doUploadAppLogo(){
			
			$imgFileName  = $_FILES['app_logo']['name'];
			$extention = strtolower(substr($imgFileName,strpos($imgFileName,'.')+1));
			$type = $_FILES['app_logo']['type'];
			
			
			$userId = $this->session->userdata('user_id');
			$uploadDir = "upload/";
			$appLogoNew = $uploadDir . $userId . "_app_logo." . $extention;	
			if (isset($_FILES['app_logo']) && ($type == 'image/jpeg'|| $type == 'image/png'))
			{
			    if (($_FILES['app_logo']['error'] > 0)) 
			    {
			        //print_r($_FILES["app_logo"]["error"]);
			        echo -1;
			    } 
			    else 
			    {
			    	//$appLogoPath = array('app_logo_path'=>addslashes(($_FILES['app_logo']['tmp_name'])));
			    	$appLogoPath = array('app_logo_path'=>$appLogoNew);
					$this->session->set_userdata($appLogoPath);
			        move_uploaded_file($_FILES['app_logo']['tmp_name'], $appLogoNew);
			        
			        echo "1";
			       
	    		}
			
			}
			
			else{
				echo -1;
			}
		}
		
		public function loadUpdateAppDesign(){
			$row_data = $this->App_Model->id_to_dbrow('app_designs','UserId',$this->session->userdata('user_id'));
			$data['upload_app_design_data'] = $row_data;
			
			$appLogoPath = array('app_logo_path' => $row_data->AppLogo);
			$this->session->set_userdata($appLogoPath);
			
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('templates_sub/popup');
			$this->load->view('app_views/app_design_views/app_home_design',$data);
			$this->load->view('templates_sub/footer');
		}
		
		
		public function sendUpdateNotification($registatoin_ids, $message) {
			$GOOGLE_API_KEY = 'AIzaSyD-lgQnANeYIZ2k6qdGmQdmO9dpXsp4R5Y';

			// Set POST variables
			$url = 'https://android.googleapis.com/gcm/send';

			$fields = array(
				'registration_ids' => $registatoin_ids,
				'data' => $message,
			); 
		

			$headers = array(
				'Authorization: key=' . $GOOGLE_API_KEY,
				'Content-Type: application/json'
			);
			// Open connection
			$ch = curl_init();

			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			// Execute post
			$result = curl_exec($ch);
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($ch));
			   
			}

			// Close connection
			curl_close($ch);
			//echo $result;
    }
		
    }
?>