<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Content_Controller extends CI_Controller {
	
	private $userId = NULL;
	private $textLabelPath = null; 
	function __construct(){
	        parent::__construct();
	        $this->load->model('Content_Model');
			$this->userId = $this->session->userdata('user_id');
    }

	public function loadTextContents()
	{
		$data['text_content'] = $this->Content_Model->getTextContent($this->userId);
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/manage_app_text_content',$data);
		$this->load->view('app_views/app_content_views/add_or_update_text_content');
		$this->load->view('templates_sub/footer');
	
	
	}
	
	public function loadVideoContents()
	{
		$result = $this->loadVideos();
		
		if($result["state"] == -1){
			$this->loadVideoContents();
		}
		
		$data["videos"] = $result["video"];
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/manage_app_video_content',$data);
		$this->load->view('app_views/app_content_views/add_video_content');
		$this->load->view('app_views/app_content_views/stream_video');
		$this->load->view('templates_sub/footer');
	
	
	}

	private function loadVideos(){
		
		$searchToken = $this->userId;
		$fields = "id,name,shortDescription,thumbnailURL,length,FLVURL";
		$readToken = "CdKLZMLbLpeOUeSduDaLWrn6eIL9n6o_EiM-wO6UCjhY9urh9BFJEQ..";
		$url = "http://api.brightcove.com/services/library?command=find_videos_by_tags&and_tags=".$searchToken."&video_fields=".$fields."&media_delivery=http&token=".$readToken;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
	
		$result = curl_exec($curl);
		if ($result === FALSE) {
			//die('Curl failed: ' . curl_error($curl));
			return array(
					'state'=>-1,
					'message'=>'Error Loading videos');
		}		
		else{
			$obj = json_decode($result,true);
			$output = array();
			if(isset($obj["items"]))
			{
				$output["video"] = $obj["items"];
			}
			$output["state"] = 1;
			return $output;	
			//echo json_encode($output);
		}
			
		curl_close($curl);
	
	}
	
	public function loadStreamingVideo($videoId){
		
		$result = $this->loadVideo($videoId);
		
		if($result["state"] == -1){
			$this->loadStreamingVideo($videoId);
		}
		$result["videoId"] = $videoId;
		$data['stream_video'] = $result;
		
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/stream_video',$data);
		$this->load->view('templates_sub/footer');
	}
	
	private function loadVideo($id){
		
		$searchToken = $this->userId;
		$fields = "name,FLVURL";
		$readToken = "CdKLZMLbLpeOUeSduDaLWrn6eIL9n6o_EiM-wO6UCjhY9urh9BFJEQ..";
		$url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_id=".$id."&video_fields=".$fields."&media_delivery=http&token=".$readToken;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt($curl, CURLOPT_TIMEOUT, 300);
	
		$result = curl_exec($curl);
		if ($result === FALSE) {
			//die('Curl failed: ' . curl_error($curl));
			return array(
					'state'=>-1,
					'message'=>'Error Loading videos');
		}		
		else{
			$output = json_decode($result,true);
			$output["state"] = 1;
			return $output;	
		}
			
		curl_close($curl);
	
	}
	
	public function updateCurrentPlayVideo(){
		$video_id = $this->input->post('video_id');
		$current_play_position = $this->input->post('current_play_position');
		$is_play = $this->input->post('is_play');
		
		if($is_play == 0){
			//request for pause
			$result = $this->Content_Model->updatePlayVideoCurrentPOsition
			($video_id,array("CurrentPlayPosition"=>$current_play_position,"NowPlaying"=>$is_play));
			
			$data = array();
			$data["currentPlayPosition"] = $current_play_position;
			$data['MessageTitle'] = 'Message_3';		
			/*Message Service Use*/
			$this->Content_Model->pushMessage($this->userId,json_encode($data),1);
		}
		else if($is_play == 1){
			//update current position
			$result = $this->Content_Model->updatePlayVideoCurrentPOsition
			($video_id,array("CurrentPlayPosition"=>$current_play_position,"NowPlaying"=>$is_play));
		}
		
		else{
			//request for play
			$result = $this->Content_Model->updatePlayVideoCurrentPOsition
			($video_id,array("CurrentPlayPosition"=>$current_play_position,"NowPlaying"=>1));
			
			$data = array();
			$data = $this->loadVideo($video_id);
			
			if($data["state"] == -1){
				$this->updateCurrentPlayVideo();
			}
			
			$data["videoId"] = $video_id;
			$data["currentPlayPosition"] = $current_play_position;
			$data['MessageTitle'] = 'Message_2';		
			/*Message Service Use*/
			$this->Content_Model->pushMessage($this->userId,json_encode($data),1);
		}
		
		
		
		
		echo $result;
	}
	
	public function loadAddNewTextContent(){
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/add_or_update_text_content');
		$this->load->view('templates_sub/footer');
	}
	
	public function loadAddNewVideoContent(){
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/add_video_content');
		$this->load->view('templates_sub/footer');
	}
	
	public function loadUpdateTextContent($id=NULL){
		$update_text_content = $this->Content_Model->id_to_dbrow('text_contents','ContentId',$id);
		$data['update_text_content'] = $update_text_content;
		
		$appIconPath = array('content_label_path'=>$update_text_content->ContentLable);
		$this->session->set_userdata($appIconPath);
		
		$this->textLabelPath = $update_text_content->ContentLable;
		//echo "<script>alert('".$this->session->userdata('content_label_path')."')</script>";
		
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('app_views/app_content_views/add_or_update_text_content',$data);
		$this->load->view('templates_sub/footer');
	
	}
		
	public function addTextContent(){
			
			$contentTitle = $this->input->post('ContentTitle');
			$contentBody = $this->input->post('ContentBody');
			
			$contentLabelPath = $this->doUploadTextContentLabel();
			
			if($contentLabelPath != -1){
				$data = array(	'ContentTitle'=>trim($contentTitle),
								'ContentBody'=>trim($contentBody),
								'ContentLable'=>$contentLabelPath,
								'ClientId'=>$this->userId
								);
				
				$result = $this->Content_Model->insertTextContent($data);
				if($result == 1)
				{
					echo 1;
				}
				else{
					echo -1;
				}
				
			
			}
			else{echo -2;}
			
	}
	
	public function updateTextContent($contentId = NULL){
			
			$contentTitle = $this->input->post('ContentTitle');
			$contentBody = $this->input->post('ContentBody');
			
			$contentLabelPath = $this->doUploadTextContentLabel();
			
			if($contentLabelPath == -1){
				$contentLabelPath = $this->input->post('htn_img');
			
			}
			
			$data = array(	'ContentTitle'=>trim($contentTitle),
							'ContentBody'=>trim($contentBody),
							'ContentLable'=>$contentLabelPath,
							'ClientId'=>$this->userId
						);
				
			$result = $this->Content_Model->updateTextContent($contentId,$data);
			if($result == 1)
			{
				echo 1;
			}
			else{
				echo -1;
			}
			
	}
	
	public function removeTextContent(){
			
			$id = $this->input->post('hdn_delete_row');
		
			$result = $this->Content_Model->deleteTextContent($id);
			if($result == 1)
			{
				echo 1;
			}
			else{
				echo -1;
			}
			
	}
	

	public function doUploadTextContentLabel(){
			
			
			if(!empty($_FILES['content_label'])){
				
				$imgFileName  = $_FILES['content_label']['name'];
				$extention = strtolower(substr($imgFileName,strpos($imgFileName,'.')+1));
				$type = $_FILES['content_label']['type'];
				
				$userId = $this->session->userdata('user_id');
				$uploadDir = 'upload/textcontent/'.$userId;
				if (!file_exists($uploadDir)) {
					mkdir($uploadDir, 0777);
				}
				
				$contentLabelNew = $uploadDir .'/'. rand(1,100) .'.'. $extention;
					
				if (isset($_FILES['content_label'])  && ($type == 'image/jpeg'|| $type == 'image/png'))
				{
					
					if (($_FILES['content_label']['error'] > 0)) 
					{
						//print_r($_FILES["content_label"]["error"]);
						return -1;
					} 
					else 
					{
						move_uploaded_file($_FILES['content_label']['tmp_name'], $contentLabelNew);
						
						return $contentLabelNew;
					   
					}
				
				}
				
				else{
					return -1;
				}
			}
			
			else{return -1;}
			
			
		}	
		
	
	public function uploadVideo(){
			$videoName = $this->input->post('videoName');
			$videoDec = $this->input->post('videoDec');
			$videoFile = $_FILES['videoFile'];
			
			$url = 'http://api.brightcove.com/services/post';
			$orgId = strtolower($this->userId);
			$videodata = array(
				'name' => $videoName,
				'shortDescription' => $videoDec,
				'tags' => array($orgId),
			);
			
			rename($videoFile["tmp_name"],$videoFile["name"]);
			$file = realpath($videoFile["name"]);
			
			$params['token'] = 'CdKLZMLbLpeOUeSduDaLWrn6eIL9n6o_Fkon-a3OPNOO8LAOi-dJ9w..';
			$params['video'] = $videodata;
			$params['encode_to'] = 'MP4';
			$post['method'] = 'create_video';
			$post['params'] = $params;
			
			
			$request['json'] = json_encode($post);

			if($file) {
				$request['file'] = '@' . $file;
			} 
		
			$curl = curl_init();

			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
			curl_setopt($curl, CURLOPT_TIMEOUT, 300);

			
			$result = curl_exec($curl);
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($curl));
				/*echo json_encode(array(
						'state'=>-1,
						'message'=>'Video Host Is Busy Now.Please Wait...'));*/
				$this->uploadVideo();
			}
			
			else{

				$arry = json_decode($result,true);
				
				$videoId = $arry['result'];
				
				if(!empty($videoId)){
					
					$data = array("VideoId"=>$videoId,"ClientId"=>$this->userId);
				
					$dbResult = $this->Content_Model->addVideo($data);
					
					echo json_encode(array(
							'state'=>1,
							'message'=>'Sucessfully Uploaded Video',
							'video_id'=>$videoId));
				}
				else{
					echo json_encode(array(
						'state'=>-1,
						'message'=>'Error Upload Video.Please Check Your File Format'));
				}
			
			}
			unlink($videoFile["name"]);
			curl_close($curl);
			
	}
}
?>