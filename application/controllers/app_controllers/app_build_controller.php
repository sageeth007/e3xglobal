<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Build_Controller extends CI_Controller {
	private $userId = NULL;
	function __construct(){
	        parent::__construct();
			$this->load->model('App_Build_Model');
			$this->userId = $this->session->userdata('user_id');
    }
	
	
	public function index()
	{	
			$this->load->view('templates_sub/header');
			$this->load->view('templates_sub/menu');
			$this->load->view('templates_sub/popup');
			$this->load->view('app_views/app_build');
			$this->load->view('templates_sub/footer');
			
			
	}
	
	public function buildApp(){
			$xml = simplexml_load_file("C:/AndroidWorkSpace/E3XGlobalApp/build.xml");
			$row = $this->App_Build_Model->getAppName('Sirasa');
			
			copy(base_url($row->AppLogo),'C:/AndroidWorkSpace/E3XGlobalApp/res/drawable-hdpi/ic_launcher.png');
			
			$xml["name"] = $row->AppName;
			$xml->asXML("C:/AndroidWorkSpace/E3XGlobalApp/build.xml"); 
			
			exec('app_build_ant.bat');
			
			echo 1;
	}
	
	public function downloadFile(){
		//Download app file
		$fullPath = "C:\AndroidWorkSpace\E3XGlobalApp\bin\Hello App1-release.apk";

		if ($fd = fopen ($fullPath, "r")) {
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
			switch ($ext) {
				case "pdf":
					header("Content-type: application/pdf"); // add here more headers for diff. extensions
					header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a download
					break;
					default;
					header("Content-type: application/octet-stream");
					header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
				}
				header("Content-length: $fsize");
				header("Cache-control: private"); //use this to open files directly
				while(!feof($fd)) {
					$buffer = fread($fd, 2048);
					echo $buffer;
				}
			}
			fclose ($fd);
	
	}
	
	
	
}