<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_Controller extends CI_Controller {

	public function index()
	{
		$this->load->view('templates_sub/header');
		$this->load->view('organizational_views/register');
		$this->load->view('templates/popup');
		$this->load->view('templates_sub/footer');
	
	}
	



	public function registerController(){
		$this->load->model('Organization_Model');
		
		$organization=$this->input->post('organization');
		$chairman=$this->input->post('chairman');
		$email=$this->input->post('email');
		$website=$this->input->post('website');
		$tpNumber=$this->input->post('tpNumber');
		$buildingNumber=$this->input->post('buildingNumber');
		$street=$this->input->post('street');
		$adrsLine2=$this->input->post('adrsLine2');
		$city=$this->input->post('city');
		$state=$this->input->post('state');
		$zipCode=$this->input->post('zipCode');
		$country=$this->input->post('country');
		$organizationType=$this->input->post('orgType');
		
		$error=0;
		
		if(empty($organization)){
		
			$error=1;
		}
		
		if(empty($chairman)){
		
			$error=1;
		}
		
		if(empty($email)){
		
			$error=1;
		}
		
		if(empty($website)){
		
			$error=1;
		}
		
		if(empty($tpNumber))
		{
			$error=1;
		}
		
		if(empty($buildingNumber)){
		
			$error=1;
		}
		
		if(empty($street)){
		
			$error=1;
		}
		
		if(empty($adrsLine2)){
		
			$error=1;
		}
		
		if(empty($city)){
		
			$error=1;
		}
		
		if(empty($state)){
		
			$error=1;
		}
		
		if(empty($zipCode)){
		
			$error=1;
		}
		
		if($error==1)
		{
			
			echo "<script> alert('Some fields are empty')</script>";
			redirect('organization_controllers/Request_Controller', 'refresh');
		}
		else {
			
			$valid=1;
			$expression = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$/";
		
			if((strlen($tpNumber)!=10)||(!(is_numeric($tpNumber)))){
				
				$valid=0;
				
				
			}
			
			if(!is_numeric($zipCode)){
				$valid=0;
			}
			
			/*if (!preg_match(" \b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b",$email)){
				$valid=0;
			}*/
			if (!preg_match($expression, $email)){
				$valid=0;
			}
			
			
			
			if($valid==1){
				$data=array('OrganizationName'=>$organization,
						'Email'=>$email,
						'ContactNumber'=>$tpNumber,
						'Chairman'=>$chairman,
						'WebSite'=>$website,
						'BuildingNumber'=>$buildingNumber,
						'Street'=>$street,
						'AddressLine2'=>$adrsLine2,
						'City'=>$city,
						'State'=>$state,
						'ZipCode'=>$zipCode,
						'Country'=>$country,
						'OrganizationType'=>$organizationType);
						
						$result = $this->Organization_Model->insertOrganization($data);
						//$success='<script> alert(Data added...) </script>'
						
						if($result==1){
							echo "<script> alert('Data added successfully ...')</script>";
							redirect('Main_Home_Controller', 'refresh');
						}
						else {
							echo "<script> alert('Data insertion failed ...');</script>";
						}
					
			}
			else{
				
				echo "<script> alert('some fields are not valid!')</script>";
				redirect('organization_controllers/Request_Controller', 'refresh');
			}
				
			
			
		}
		
		
		
		//echo "From Organization input : {$organization}<br/>";
		
		/*echo $this->db->query('insert into oraganization_users values ($email,$tpNumber,$chairman,$website,
							$buildingNumber,$street,$adrsLine2,$city,$state,$zipCode,$country,$organizationType)');*/
							
		
					
			}
		
		
}
?>