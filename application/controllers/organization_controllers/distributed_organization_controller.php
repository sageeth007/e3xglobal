<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributed_Organization_Controller extends CI_Controller {
	
	private $userId = NULL;
	function __construct(){
	        parent::__construct();
	        $this->load->model('Organization_Model');
			$this->userId = $this->session->userdata('user_id');
    }

	public function loadManageMapPlaces()
	{
		$data['map_place'] = $this->Organization_Model->getMapPlaces($this->userId);
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('organizational_views/manage_map_places',$data);
		$this->load->view('app_views/app_build',$data);
		$this->load->view('templates_sub/footer');
	
	
	}
	
	public function loadAddNewMapPlace(){
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('organizational_views/add_new_map_place');
		$this->load->view('templates_sub/footer');
	}
		
	public function addMapPlace(){
			
			$p_lat = $this->input->post('p_lat');
			$p_lng = $this->input->post('p_lng');
			$p_addr = $this->input->post('p_addr');
			$p_name = $this->input->post('p_name');
		
			
				
			$data = array(	'OrgId'=>$this->userId,
							'BranchAddress'=>trim($p_addr),
							'BranchName'=>trim($p_name),
							'Branch_Lat'=>$p_lat,
							'Branch_Lng'=>$p_lng);
							
			$result = $this->Organization_Model->addMapPlace($data);
			
			echo $result;
	}

	public function loadManagePromations($branchId){
		
		$data['branch'] = $this->Organization_Model->id_to_dbrow('distributed_branch','BranchId',$branchId);
		$data['promotions'] = $this->Organization_Model->getPromotions($branchId);
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('organizational_views/manage_promotions',$data);
		$this->load->view('templates_sub/footer');
		
	}
	
	public function loadAddNewPromotion($proId = null,$branchId){
		
		$data['branch'] = array('branch_id' => $branchId);
		$promotion = $this->Organization_Model->id_to_dbrow('branch_promotion','PromId',$proId);
		$data['promotion'] = $promotion;
		
		if($proId != -1){
			$this->session->set_userdata('content_label_path',$promotion->ImagePath);
		}
		
		$this->load->view('templates_sub/header');
		$this->load->view('templates_sub/menu');
		$this->load->view('templates_sub/popup');
		$this->load->view('organizational_views/add_or_update_promotion',$data);
		$this->load->view('templates_sub/footer');
	}
	
	public function addPromotion($proId = NULL){
			
			$Title = $this->input->post('Title');
			$Content = $this->input->post('Content');
			$ImagePath = $this->session->userdata('content_label_path');
			$BranchId = $this->input->post('BranchId');
		
			$data = array(	'Title'=>trim($Title),
							'Content'=>trim($Content),
							'ImagePath'=>$ImagePath,
							'BranchId'=>$BranchId
							);
			
			if(empty($proId)){
				$result = $this->Organization_Model->addPromation($data);
				if($result == 1)
				{
					
					$data['branch'] = $this->Organization_Model->id_to_dbrow('distributed_branch','BranchId',$BranchId);
					$data['promotions'] = $this->Organization_Model->getPromotions($BranchId);
					$this->load->view('templates_sub/header');
					$this->load->view('templates_sub/menu');
					$this->load->view('templates_sub/popup');
					$this->load->view('organizational_views/manage_promotions',$data);
					$this->load->view('templates_sub/footer');
				}
				else{

					echo "<script>alert('Error add text content')</script>";
				}
			}
			else{
				$result = $this->Organization_Model->updatePromotion($proId,$data);
				if($result == 1)
				{
					$data['branch'] = $this->Organization_Model->id_to_dbrow('distributed_branch','BranchId',$BranchId);
					$data['promotions'] = $this->Organization_Model->getPromotions($BranchId);
					$this->load->view('templates_sub/header');
					$this->load->view('templates_sub/menu');
					$this->load->view('templates_sub/popup');
					$this->load->view('organizational_views/manage_promotions',$data);
					$this->load->view('templates_sub/footer');
				}
				else{
					$this->session->unset_userdata('selected_branch');
					echo "<script>alert('Error Edit content')</script>";
				}
			}
			
			
			
	}

	function doUploadContentLabel(){
			
			$imgFileName  = $_FILES['content_label']['name'];
			$extention = strtolower(substr($imgFileName,strpos($imgFileName,'.')+1));
			$type = $_FILES['content_label']['type'];
			
			$userId = $this->session->userdata('user_id');
			$uploadDir = 'upload/promotions/'.$userId;
			if (!file_exists($uploadDir)) {
				mkdir($uploadDir, 0777);
			}
			
			$contentLabelNew = $uploadDir .'/'. rand(1,100) .'.'. $extention;
				
			if (isset($_FILES['content_label'])  && ($type == 'image/jpeg'|| $type == 'image/png'))
			{
				
			    if (($_FILES['content_label']['error'] > 0)) 
			    {
			        //print_r($_FILES["content_label"]["error"]);
			        echo -1;
			    } 
			    else 
			    {
			    	$appIconPath = array('content_label_path'=>$contentLabelNew);
					$this->session->set_userdata($appIconPath);
					
			        move_uploaded_file($_FILES['content_label']['tmp_name'], $contentLabelNew);
			        
			        echo 1;
			       
	    		}
			
			}
			
			else{
				echo -1;
			}
			
		}	
	
	
}
?>