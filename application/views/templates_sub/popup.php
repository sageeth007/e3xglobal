<!--Error -->
<div class="alert alert-danger alert-dismissable" id="alert_error" style="display: none">
  		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		<strong>Warning!</strong> <span id="msg_body_error"></span>
</div>
<!--Sucess -->
<div class="alert alert-success alert-dismissable" id="alert_sucess" style="display: none">
  		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  		<strong>Sucess</strong> <span id="msg_body_sucess"></span>
</div>
