<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
      	
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">E3XGlobal</a>
        </div>
        
        <div class="navbar-collapse collapse">
          
          <ul class="nav navbar-nav">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-th-large"></span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                
                <?php 
	                                		$menu_array = $this->session->userdata('menu');
	                                		foreach($menu_array as $row)
											{
										?>
		                                    <li>
		                                        <a  href="<?php echo site_url($row[1])?>"><?php echo $row[0]?></a>
		                                        
		                                    </li>
		         <?php
											}
		         ?>
		         
               </ul>
            </li>
			
          </ul>
          
      	 <ul class="nav navbar-nav navbar-right">
       
	       <li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Welcome <?php echo ucfirst($this->session->userdata('user_name')); ?> <b class="caret"></b></a>
	        <ul class="dropdown-menu">
	          <li><a href="#">Settings</a></li>
	          <li role="presentation" class="divider"></li>
	          <li><a href="<?php echo site_url('Main_Home_Controller/logout'); ?>">Logout</a></li>
	        </ul>
	      </li>                          
    
    	</ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	<br>
	<br>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 main">
        	</br>
        	 
            <?php if($this->session->userdata('msg-success')) { ?>
	       		<div class="alert alert-success center">
	       		<?php 
	       			echo $this->session->userdata('msg-success'); 
					$this->session->unset_userdata('msg-success'); 
	       		?>
	       		</div>
	       <?php } ?>
	       
	       <?php if($this->session->userdata('msg-error')) { ?>
		       <div class="alert alert-danger center">
		       	<?php  	
		       			echo validation_errors(); 
		       	  		echo $this->session->userdata('msg-error');
		       			$this->session->unset_userdata('msg-error'); 
		       	?>
		       </div>
	       <?php } ?>
        	