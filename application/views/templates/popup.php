<!--Sucess -->
<div id="dialog_sucess" title="Sucess" style="display:none">
  	<p>
    <span class="" style="float:left; margin:0 7px 20px 0;">
    <img src="<?php echo base_url('assets/site/images/ok.png')?>" width="32" height="32" id="img_sucess"/>
    </span>
    <span id="span_popup_sucess"></span>
    </p>
</div>
<!--Warning -->
<div id="dialog_warning" title="Warning !" style="display:none">
  	<p>
    <span class="" style="float:left; margin:0 7px 20px 0;">
    <img src="<?php echo base_url('assets/site/images/warning.png')?>" width="32" height="32" id="img_warning"/>
    </span>
    <span id="span_popup_warning"></span>
    </p>
</div>

<!--Error -->
<div id="dialog_error" title="Error !" style="display:none">
  	<p>
    <span class="" style="float:left; margin:0 7px 20px 0;">
    <img src="<?php echo base_url('assets/site/images/err.png')?>" width="32" height="32" id="img_error"/>
    </span>
    <span id="span_popup_error"></span>
    </p>
</div>

<!--Confirm -->
<div id="dialog_confirm" title="Warning !" style="display:none">
  	<p>
    <span class="" style="float:left; margin:0 7px 20px 0;">
    <img src="<?php echo base_url('assets/site/images/warning.png')?>" width="32" height="32" id="img_confirm"/>
    </span>
    <span id="span_popup_confirm"></span>
    </p>
</div>    