<script>
	var player;
	function setPreviewVideo(name,url){
		document.getElementById('b_pre').innerHTML = name;
		player = document.getElementById('player');
		document.getElementById('video_src').setAttribute('src',url);
		player.pause();
		player.load();
     
		$('#myModal').modal('show');
	}
	
	function playerStop(){
		player.pause();
	}

</script>
<h1><i class="icon-reorder"></i>App Contents
</h1>
	<ul class="nav nav-tabs">
    <li ><a href="<?php echo site_url('app_controllers/app_content_controller/loadTextContents') ?>">Manage Text Content</a></li>
    <li class="active"><a href="#">Manage Video Content</a></li>
	<li ><a href="#">Manage Audio Content</a></li>
	</ul>
	
	
	<div class="panel panel-primary">
	    <div class="panel-heading" >
	        <h3 class="panel-title">Manage Video Content</h3>
			
	    </div>
	    
	    <div class="panel-body">
				<a 
				href="javascript:$('#modalVideoContent').modal('show')" 
				style="float:right"
				class="btn btn-sm btn-info">
				NEW&nbsp;
				<span class="glyphicon glyphicon-film"></span>
				</a>
	        	<div class="row" align="center"> 
			       
					<?php 
						if(sizeof($videos) > 0){
						
					?>
						<table class="table table-hover" style="width: 900px">
							<thead>
								<tr>
								<th style="width: 150px;">Video Id</th>							
								<th style="width: 250px;">Video Name</th>
								<th style="width: 260px;">Video Desc</th>
								<th style="width: 150px;">Thumbnail</th>
								<th style="width: 150px;">length</th>	
								<th style="width: 400px;">Action</th>
								</tr>								
							</thead>								
							<tbody>								
										
										
							<?php	
								
								foreach ($videos as $key => $row) { ?>	
													
								<tr>
								<td ><?php echo $row["id"] ?></td>
								<td ><?php echo $row["name"] ?></td>
								<td >
									<div style="height:60px;overflow-y:auto">
										<?php echo $row["shortDescription"] ?>
									</div>
								</td>
								<td >
																
									<img   
									id="content_label_pre" 
									style="background-color:#fff;display:block;width:60px;height: 60px" 
									src="<?php  echo $row["thumbnailURL"] ?>"
									/>
															
															
								</td>
								<td >
									<?php echo ($row["length"]/1000)." s"?>
								</td>
								<td >
									<a 
										href="javascript:setPreviewVideo('<?php echo $row["name"] ?>','<?php echo $row["FLVURL"] ?>')" 
										class="btn btn-sm btn-info">
										<span class="glyphicon glyphicon-play"></span>
									</a>
									
									<a 
										href="javascript:initStream('<?php echo $row["id"] ?>','<?php echo $row["name"] ?>','<?php echo $row["FLVURL"] ?>')" 
										class="btn btn-sm btn-warning">
										<span class="glyphicon glyphicon-cloud"></span>
									</a>
									
									<a href="#" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-edit"></span></a>
									<a href="#" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
																
								</td>
								</tr>
													
									<?php }}
												 
											else{
									?>
											<div style="width: 200px" class="alert alert-warning">No Any Video Content Aviable</div>
									<?php		
											}		
												
									?>
								</tbody>
								</table>
			        			
		        </div>
		               
	    </div>
	   
	    
</div>

<div class="form-group" id="button" align="center" >
		<button style="" onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadManageMapPlaces') ?>'" class="btn btn-success "  >
		<span class=""></span>Go To Places</button>
									  
		<button id="btn_next" onclick="window.location='<?php echo site_url('app_controllers/app_design_controller') ?>'" class="btn btn-default "   >
	    <span class=""></span>Back</button>
									  
</div>

<div class="modal fade" id="myModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="playerStop()">&times;</button>
        <h4 class="modal-title">Preview Video : <b id="b_pre"></b></h4>
      </div>
      <div class="modal-body">
			<video id="player" controls style="width:530px;height:368px">
				<source id="video_src"  type="video/mp4">
			</video>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


			
	
	
	
	
