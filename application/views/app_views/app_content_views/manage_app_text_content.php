<h1><i class="icon-reorder"></i>App Contents
</h1>
	<ul class="nav nav-tabs">
    <li class="active"><a href="#">Manage Text Content</a></li>
    <li ><a href="<?php echo site_url('app_controllers/app_content_controller/loadVideoContents') ?>">Manage Video Content</a></li>
	<li ><a href="#">Manage Audio Content</a></li>
	</ul>
	
	
	<div class="panel panel-primary">
	    <div class="panel-heading" >
	        <h3 class="panel-title">Manage Text Content</h3>
			
	    </div>
	    
	    <div class="panel-body">
				<a 
				href="javascript:setAddTextContentForm()" 
				style="float:right"
				class="btn btn-sm btn-info">
				NEW&nbsp;
				<span class="glyphicon glyphicon-file"></span>
				</a>
				
	        	<div class="row" align="center"> 
			        			<?php 
												$is_have_place = $text_content->num_rows();
												if($text_content->num_rows() > 0){
								?>
							        			<table class="table table-hover" style="width: 800px">
													<thead>
														<tr>
																<th style="width: 150px;">Content Title</th>							
																<th style="width: 150px;">Content Body</th>
																<th style="width: 150px;">Content Lable</th>
																<th  style="width: 150px;">Action</th>															
														</tr>								
													</thead>								
													<tbody>								
										
										
										<?php		
														foreach ($text_content->result() as $row) { ?>	
														
														<tr>
															<td ><?php echo $row->ContentTitle; ?></td>
															<td ><?php echo $row->ContentBody; ?></td>
															<td >
																
																<img   
																
																style="background-color:#fff;display:block;width:60px;height: 60px" 
																src="<?php  echo base_url($row->ContentLable);?>"
																/>
															
															
															</td>
															<td >
																<a href="javascript:setUpdateTextContentForm
																('<?php echo $row->ContentId ?>','<?php echo $row->ContentTitle ?>','<?php echo $row->ContentBody ?>','<?php echo base_url($row->ContentLable) ?>','<?php echo $row->ContentLable ?>')" 
																class="btn btn-sm btn-success">
																<span class="glyphicon glyphicon-edit">
																</span>
																</a>
																<a href="javascript:setUpRemoveContent('<?php echo $row->ContentId ?>')" class="btn btn-sm btn-danger ask-delete"><span class="glyphicon glyphicon-remove"></span></a>
																
															</td>
														</tr>
													
										<?php }}
												 
												else{
										?>
													<div style="width: 200px" class="alert alert-warning">No Any Text Content Aviable</div>
										<?php		
												}		
												
										 ?>
										
									</tbody>
								</table>	
									
		            </div>
					               
	    </div>
		
</div>

<div class="form-group" id="button" align="center" >
		<button style="" onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadManageMapPlaces') ?>'" class="btn btn-success "  >
		<span class=""></span>Go To Places</button>
									  
		<button id="btn_next" onclick="window.location='<?php echo site_url('app_controllers/app_design_controller') ?>'" class="btn btn-default "   >
	    <span class=""></span>Back</button>
									  
</div>
			
	
	
	
	
