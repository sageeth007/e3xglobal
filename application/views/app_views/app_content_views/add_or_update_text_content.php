	<!-- Modal -->
	<div class="modal fade" id="modalTextContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form id="form_upload_text_content" method="post" enctype="multipart/form-data" class="form-horizontal">
		      <div class="modal-header btn-primary" >
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Add Text Content</h4>
		      </div>
	      
		      <div class="modal-body " >
			  
								<div class="form-group" id="div_app_name_container">
													<label for="textfield" class="col-md-4 control-label">Content Title : </label>
													<div class="input-group col-md-6 "> 
															<input 
															value=""
															class="form-control" 
															type="text"    
															placeholder="content title" 
															name="ContentTitle" 
															id="ContentTitle" 
															required="true"/>                                                                             
															
													</div>
								</div>
			        			
			        			<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">Body : </label>
                                            <div class="input-group col-md-6"> 
													<textarea class="form-control" rows="5"  id="ContentBody"  name="ContentBody" required="required" ></textarea>
                                                                                                                               
                                            </div>
                                </div>
                               
								<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">Image : </label>
                                            <div class="input-group col-md-6"> 
												<input type="file" id="content_label" name="content_label" required="true" onchange="preview_image()" class="form-control"/>                      	                                                      
                                            </div>
                                            
                                           	
                                </div>
								
								<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label"></label>
                                            <div class="input-group col-md-6" align="right"> 
											    <img   
													id="content_label_pre" 
													style="background-color:#fff;display:block;width:60px;height: 60px" 
													src="<?php echo base_url('assets/site/images/no_image.png') ?>"
												/>
												
                                            </div>
                                </div>
								
								<input type="hidden" name="htn_img" id="htn_img" />
					<br></br>
		            <div class="form-group" id="" align="center">
		                 
		                 <div class="progress "  style="width: 500px">
  						 <div align="center" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
						    0%
						 </div>
						 </div>
		                    
		            </div>
			        
			 </div>
	     	
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Upload</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
	
	<!-- Confirm Delete Modal -->
	<div id="confirm_box_modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form id="form_remove_text_content" method="post" action="<?php echo site_url('app_controllers/app_content_controller/removeTextContent')?>">
				 <div class="modal-header"  >
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Warning</h4>
					
				 </div>
				 <div class="modal-body"  align="center">
					<b>Are you sure.Do you want to remove this content ?</b>
					<input type="hidden" id="hdn_delete_row" name="hdn_delete_row"/>
				 </div>
				 <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Remove</button>
				 </div>
			 </form> 
		</div>
	  </div>
	</div>
	
	<!-- img upload ajax -->
	<script src="<?php echo base_url('assets/site/js/jquery.js')?>"></script>
	<script src="<?php echo base_url('assets/site/js/jquery.form.js')?>"></script>
	
	<script>
		
		function setAddTextContentForm(){
			var form = document.getElementById('form_upload_text_content');
			var actionURL = '<?php echo site_url('app_controllers/app_content_controller/addTextContent')?>';
			form.action = actionURL;
			javascript:$('#modalTextContent').modal('show');
		}
		
		function setUpdateTextContentForm(id,title,body,imgUrl,urlDB){
			var form = document.getElementById('form_upload_text_content');
			var actionURL = '<?php echo site_url('app_controllers/app_content_controller/updateTextContent')?>';
			form.action = actionURL + '/' + id;
			document.getElementById('ContentTitle').value = title;
			document.getElementById('ContentBody').innerHTML = body;
			document.getElementById('content_label_pre').src = imgUrl;
			document.getElementById('htn_img').value = urlDB;
			document.getElementById('content_label').removeAttribute('required');
			javascript:$('#modalTextContent').modal('show');
		}
		
		function preview_image(){
				
			    // get selected file element
			    var oFile = document.getElementById('content_label').files[0];
			
			    // get preview element
			    var oImage = document.getElementById('content_label_pre');
			
			    // prepare HTML5 FileReader
			    var oReader = new FileReader();
			        oReader.onload = function(e){
					
			        // e.target.result contains the DataURL which we will use as a source of the image
			        oImage.src = e.target.result;
			
			        oImage.onload = function () { // binding onload event
						
			            // we are going to display some custom image information here
			            
			        };
					
					
			    };

			    // read selected file as DataURL
			    oReader.readAsDataURL(oFile);
				
		}

		//Add or Update text Content
        (function() {
 
            var progress_bar = $('.progress-bar');
 
            $('#form_upload_text_content').ajaxForm({
                beforeSend: function() {
                    //status.empty();
                    var percentVal = '0%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                success: function() {
                    var percentVal = '100%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                complete: function(xhr) {
                    //status.html(xhr.responseText);
                    $('#modalTextContent').modal('hide');
                   
                   	if(xhr.responseText == '1'){
                   		sucess_message('Sucess fully submit data');
						window.location = '<?php echo site_url('app_controllers/app_content_controller/loadTextContents')?>';
                   	}
                   	else if(xhr.responseText == '-2'){
                   		
                   		document.getElementById('content_label_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		error_message('Error upload app image.');
                   		var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
                   	}
					else{
						
						document.getElementById('content_label_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		error_message('Error Submit Data.Please Fill All Fields');
                   		var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
					}
                    
                    
                    
                }
            });
        })();
		
		
		function setUpRemoveContent(id){
			document.getElementById('hdn_delete_row').value = id;
			javascript:$('#confirm_box_modal').modal('show');
		}
		
		//Remove text sontent
		(function() {
 
			$('#form_remove_text_content').ajaxForm({
                
                complete: function(xhr) {
                    //status.html(xhr.responseText);
                    $('#confirm_box_modal').modal('hide');
                   
                   	if(xhr.responseText == '1'){
                   		sucess_message('Sucess fully remove data');
						window.location = '<?php echo site_url('app_controllers/app_content_controller/loadTextContents')?>';
                   	}
					else{
						
						document.getElementById('hdn_delete_row').value = '';
                   		error_message('Error remove data.');
					}
                    
                    
                    
                }
            });
        })();
		
		
		$('#modalTextContent').on('hidden.bs.modal', function (e) {
			document.getElementById('ContentTitle').value = '';
			document.getElementById('ContentBody').innerHTML = '';
			document.getElementById('content_label_pre').src = '';
			document.getElementById('htn_img').value = '';
			document.getElementById('content_label').setAttribute('required','required');
		
		});
		
		$('#confirm_box_modal').on('hidden.bs.modal', function (e) {
			document.getElementById('hdn_delete_row').value = '';
		});
		
    </script>
    

	<!-- end image upload ajax -->
	
	
	     	
	


	