	
	<!-- Modal -->
	<div class="modal fade"  id="modalVideoContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form 
			id="form_video_content"
			method="post"  
			class="form-horizontal" 
			action="<?php echo site_url('app_controllers/app_content_controller/uploadVideo') ?>"
			enctype="multipart/form-data"
		
		>
		      <div class="modal-header btn-primary" style="">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Add Video Content</h4>
		      </div>
	      
		      <div class="modal-body" >
							<div class="form-group" id="div_app_name_container">
											<label for="textfield" class="col-md-4 control-label">Video Name : </label>
                                            <div class="input-group col-md-6 "> 
                                                    <input 
													class="form-control" 
													type="text"    
													placeholder="Video Name" 
													name="videoName" 
													id="videoName" 
													required="true"/>                                                                             
													
											</div>
                                </div>
			        			
			        			<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">Video Description : </label>
                                            <div class="input-group col-md-6"> 
                                                    <textarea 
													class="form-control" 
													placeholder="Video Description" 
													name="videoDec" 
													id="videoDec" 
													rows="5" 
													required="true">
													</textarea>                                                                             
                                            </div>
                                </div>
                               
								
								
								<div class="form-group" id="div_app_description_container">
									<label for="textfield" class="col-md-4 control-label">Video : </label>
									<div class="input-group col-md-6"> 
										<input type="file" id="videoFile" name="videoFile" class="form-control" required="true"/>                      	                                                      
										<div style="float:right" id="progress_bar_content" ></div>
									</div>
									
								</div>
                                
								<br></br>
								<div class="form-group" id="" align="center">
		                 
									 <div class="progress "  style="width: 500px">
									 <div align="center" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
										0%
									 </div>
									 </div>
		                    
								</div>
								
			        
			 </div>
	     	
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Upload</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
	
	
	<script src="<?php echo base_url('assets/site/circular-progress-master/circular-progress.js')?>"></script>
	<script src="<?php echo base_url('assets/site/js/jquery.js')?>"></script>
	<script src="<?php echo base_url('assets/site/js/jquery.form.js')?>"></script>
	
    <script>
		
		
        (function() {
			var progress_bar = $('.progress-bar');
            $('#form_video_content').ajaxForm({
                beforeSend: function() {
					var percentVal = '0%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal); 
                },
                uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                success: function() {
                    var percentVal = '100%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                complete: function(xhr) {
					 $('#modalVideoContent').modal('hide');
                    var obj = JSON.parse(xhr.responseText);
					
                   	if(obj.state == '1'){
                   		sucess_message(obj.message + ", Video Id : " + obj.video_id);
						//window.location = '<?php echo site_url('app_controllers/app_content_controller/loadVideoContents') ?>';
                   	}
                   	else{
                   		error_message(obj.message);
                   		document.getElementById('progress_bar_content').innerHTML = "";
						var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
                   	}
                    
                    
                    
                }
            });
        })();
    </script>