<div class="modal fade" id="streamVideoModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Streming : <b id="s_pre"></b></h4>
      </div>
      <div class="modal-body">
			<video id="player_stream" controls style="width:530px;height:368px">
						<source id="video_stream_src"  type="video/mp4">
			</video>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	var playerS;
	var interval;
	var videoSource;
	var videoId;
	function initStream(vId,vName,vUrl){
		
		videoId = vId;
		document.getElementById('s_pre').innerHTML = vName + ' [' + videoId + ']';
		 
		playerS = document.getElementById('player_stream');
		playerS.addEventListener('play', onPlay, false);
		playerS.addEventListener('pause', onPause, false);
	
		videoSource = document.getElementById('video_stream_src');
		videoSource.setAttribute('src',vUrl);
		
		playerS.load();
		$('#streamVideoModal').modal('show');
	
	}
	
	function onPlay(){
		updateCurrentTime(parseInt(playerS.currentTime, 10),3);
		interval = setInterval(function(){showSeek()},1000);
	}
		
	function onPause(){
		clearInterval(interval);
		updateCurrentTime(parseInt(playerS.currentTime, 10),0);
		
	}
		
	function showSeek(){
		updateCurrentTime(parseInt(playerS.currentTime, 10),1);
	}
	
	function updateCurrentTime(current_play_position,is_play){
		var video_id = videoId;
		$.ajax({
		        	'url' : '<?php echo site_url('app_controllers/App_Content_Controller/updateCurrentPlayVideo');?>',
		            'type' : 'POST', 
		            'data' : {	'video_id' : video_id,
								'current_play_position' : current_play_position,
								'is_play':is_play
							  },
		            success: function(data) {
		            	
		            	if(data == 1){
		            		
						}
		            	else if(data == -1){
		            		
		            	}
		            	
		            },
		});
	}
	
	$('#streamVideoModal').on('hidden.bs.modal', function (e) {
			playerS.pause();
	});

	
</script>	
	