<script>
			
		    var save_layout_url = '<?php echo site_url('app_controllers/app_design_controller/saveLayout');?>';
		    
		    
		    $(document).ready(function(){
		    	var color = document.getElementById('body_bar_color').value;
				$("#app_actionbar").css("background-color", "#"+color);
				
				var color = document.getElementById('action_bar_color').value;
				$("#app_body").css("background-color", "#"+color);
		    	
		    	init();
				
				initLbles();
		    });
		
			function set_color(get_element,set_element)
			{
				//alert(set_element+"..."+get_element);
				var color = document.getElementById(get_element).value;
				$("#"+set_element).css("background-color", "#"+color);
			}
			
			
			function preview_image(){
				
			    // get selected file element
			    var oFile = document.getElementById('app_logo').files[0];
			
			    // get preview element
			    var oImage = document.getElementById('app_logo_pre');
				var oImage_emu = document.getElementById('emu_app_logo_pre');
			
			    // prepare HTML5 FileReader
			    var oReader = new FileReader();
			        oReader.onload = function(e){
			
			        // e.target.result contains the DataURL which we will use as a source of the image
			        oImage.src = e.target.result;
					oImage_emu.src = e.target.result;
					
			        oImage.onload = function () { // binding onload event
			
			            // we are going to display some custom image information here
			            
			        };
			    };

			    // read selected file as DataURL
			    oReader.readAsDataURL(oFile);
				
			}
		
		    
			
			function save_layout(){
				
				if(checkChangeLayOut() == -1){return;}
				
				var app_id = "<?php echo !empty($upload_app_design_data->AppId)?$upload_app_design_data->AppId:NULL?>";
				var app_name = document.getElementById("p_app_name").innerHTML;
				var app_desc = "<?php echo !empty($upload_app_design_data->ActionBarColor)?$upload_app_design_data->AppDescription:$this->session->userdata('app_description') ?>";
				var action_bar_color = "#"+document.getElementById("action_bar_color").value;
				var app_body_color = "#"+document.getElementById("body_bar_color").value;
				var contents_ads_v = ((document.getElementById("hdn_contents_ads_v").value)/300)*10;
				
				$.ajax({
		        	'url' : save_layout_url,
		            'type' : 'POST', //the way you want to send data to your URL
		            'data' : {	'app_id' : app_id,
								'app_name' : app_name,
								'app_desc' : app_desc ,
								'action_bar_color' : action_bar_color,
								'app_body_color':app_body_color,
								'contents_ads_v' : Math.round(contents_ads_v)
							  },
		            success: function(data) {
		            	
		            	if(data == 1){
		            		sucess_message('Sucess fully submitted app design....');
							document.getElementById('btn_next').disabled=false;
						}
		            	else if(data == -1){
		            		error_message('DB_Error.....');
		            	}
		            	else if(data == -2){
		            		error_message('Please upload app logo....');
		            	}
		            	
		            	else{
		            		alert(data);
		            	}
		            },
		       	});
		       	return false;
			}   

			
			function initLbles(){
				
				hdn = document.getElementById('hdn_contents_ads_v').value;
				
				document.getElementById('lbl_ads').innerHTML =
				"Content : "+Math.round((hdn/300)*10)+" Ads : "+(10 - Math.round((hdn/300)*10));;
				
			}
			
			function checkChangeLayOut(){
				var ct =  document.getElementById('hdn_contents_ads_correct').value;
				
				if(ct == 'true'){
					return 1;
				}
				else{
					error_message('Error changeing layout....');
					return -1;
				}
			}
			
			</script>

<link href="<?php echo base_url('assets/site/color_picker/build/1.2.0/css/pick-a-color-1.2.0.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/site/css/tab_css.css')?>" rel="stylesheet">

<div class="panel panel-primary" id="menu_design" >
	<div class="panel-heading">
	        <h3 class="panel-title">Change the menu layout</h3>
	</div>
	
	<div class="panel-body">
			
			<div class="row">
			<div class="col-md-6 form-horizontal">
				<br>
				<br>
				<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Action Bar Color : </label>
                    <div class="input-group col-md-4"> 
                    	<input readonly onchange="set_color('action_bar_color','app_actionbar')" id="action_bar_color" type="text" 
                    	value="<?php echo !empty($upload_app_design_data->ActionBarColor)?$upload_app_design_data->ActionBarColor:'222' ?>" name="border-color" class="pick-a-color form-control">                                                   
               		</div>
                </div>
                
                <div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Body Color: </label>
                    <div class="input-group col-md-4"> 
                    	<input readonly onchange="set_color('body_bar_color','app_body')" id="body_bar_color" type="text" 
                    	value="<?php echo !empty($upload_app_design_data->BodyColor)?$upload_app_design_data->BodyColor:'222' ?>" name="border-color" class="pick-a-color form-control">                                                   
               		</div>
                </div>
				
				<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">App Icon: </label>
                    <div class="input-group col-md-4"> 
                    	<a href="javascript:$('#myModal').modal('show')"><img   id="app_logo_pre" style="background-color:#fff;display:block;width:60px;height: 60px" 
                               src="<?php echo !empty($upload_app_design_data->ActionBarColor)?base_url($upload_app_design_data->AppLogo):base_url('assets/site/images/no_image.png') ?>"/></a>
               		</div>
                </div>
				
				
				 
				 <div class="form-group" id="ads_tab_indi">
					<label for="textfield" class="col-md-4 control-label">Advertiment Tab : </label>
                    <div class="input-group col-md-4 "> 
					    <label class="form-control" id="lbl_ads" width="50px">OOK</label>
                     </div>
                 </div>
				
				</br>
				<div class="form-group" id="button" align="center" >
					<button style="" onclick="save_layout()" class="btn btn-success "  >
					<span class="glyphicon glyphicon-floppy-disk"></span>Save Layout</button>
                      
					<button id="btn_next" onclick="window.location='<?php echo site_url('app_controllers/App_Content_Controller/loadTextContents') ?>'" class="btn btn-warning "   >
					<span class=""></span>Go To Contents</button>
					  
                </div>
				
				<input type="hidden" id="hdn_contents_ads_v" value="200"/>
				<input type="hidden" id="hdn_contents_ads_correct"  value="true"/>
				
			</div>
			
			<div class="col-md-6">
			<div  id="emulator_containor" align="center"  >
				
				<div id="emulator_background" align="center">
					
					<div id="emulator_speker"></div>
					<div id="app_background" align="center">
						<div id="app_actionbar" 
							
							style="background-color: <?php echo !empty($upload_app_design_data->ActionBarColor)?$upload_app_design_data->ActionBarColor:'222' ?>"
						
						>
							<div id="app_logo_container">
								<a style="float: left;margin-left: 2px" title="Select the logo">
									<img id="emu_app_logo_pre" src="<?php echo !empty($upload_app_design_data->ActionBarColor)?base_url($upload_app_design_data->AppLogo):base_url('assets/site/images/no_image.png') ?>" width="40px" height="28px"/>
								</a>
							</div>
							
							<div style="float: right;margin-right: 2px;color:#FFFFFF" id="app_name">
								<b><p id="p_app_name"><?php echo !empty($upload_app_design_data->AppName)?$upload_app_design_data->AppName:$this->session->userdata('app_name') ?></p></b>
							</div>
						</div>
						
					
						
					<div id="app_body" 
							style="background-color: <?php echo !empty($upload_app_design_data->BodyColor)?$upload_app_design_data->BodyColor:'222' ?>"
						 >
						
						<div id="navcontainer">
						<ul>
						<li><a href="#">Text</a></li>
						<li><a href="#">Video</a></li>
						<li><a href="#">Audio</a></li>

						</ul>
						</div>
						<div id='canvas_contener'>
							<canvas id='canvas' height='300px' width='264px'>
						This text is displayed if your browser does not support HTML5 Canvas.
							</canvas>
						</div>
					</div>
					</div>
					
					<div id="emulator_home_btn"></div>
				</div>
				
			
		</div>
		
				 
				 	
		</div>
		
		</div>
		
	</div>
	
	
</div>
<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form action="<?php echo site_url('app_controllers/app_design_controller/doUploadAppLogo')?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Upload App Logo</h4>
		      </div>
	      
		      <div class="modal-body" >
		      	
			      	<div class="form-group" id="div_app_description_container">
		                 <label for="textfield" class="col-md-4 control-label">Icon : </label>
		                 <div class="input-group col-md-6"> 
		                      <input type="file" id="app_logo" name="app_logo" onchange="preview_image()" class="form-control"/>                      	                                                      
		                    
		                 </div>
		                
		                    
		            </div>
		            <div class="form-group" id="" align="center">
		                 
		                 <div class="progress "  style="width: 500px">
  						 <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="">
						    0%
						 </div>
						 </div>
		                    
		            </div>
			        
			 </div>
	     	
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Upload</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
<!-- End Model --> 


	
<!-- color picker -->
	<script src="<?php echo base_url('assets/site/color_picker/build/dependencies/jquery-1.9.1.min.js')?>"></script>
	<script src="<?php echo base_url('assets/site/color_picker/build/dependencies/tinycolor-0.9.15.min.js')?>"></script>
	<script src="<?php echo base_url('assets/site/color_picker/build/1.2.2/js/pick-a-color-1.2.2.min.js')?>"></script>	
	
	
	<script type="text/javascript">
	
		$(document).ready(function () {

			$(".pick-a-color").pickAColor({
			  showSpectrum            : true,
				showSavedColors         : true,
				saveColorsPerElement    : true,
				fadeMenuToggle          : true,
				showAdvanced						: true,
				showBasicColors         : true,
				showHexInput            : true,
				allowBlank							: true
			});
			
		});
	
	</script>
<!-- end color picker -->


<!-- img upload ajax -->
	
	<script src="<?php echo base_url('assets/site/js/jquery.form.js')?>"></script>
	
    <script>
        (function() {
 
            var progress_bar = $('.progress-bar');
 
            $('form').ajaxForm({
                beforeSend: function() {
                    //status.empty();
                    var percentVal = '0%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                success: function() {
                    var percentVal = '100%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                complete: function(xhr) {
                    //status.html(xhr.responseText);
                    $('#myModal').modal('hide');
                   
                   	if(xhr.responseText == '1'){
                   		sucess_message('Sucess fully upload logo');
                   		document.getElementById('emu_app_logo_pre').src = document.getElementById('app_logo_pre').src; 
                   	}
                   	else{
                   		//alert(xhr.responseText);
                   		document.getElementById('app_logo_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		document.getElementById('emu_app_logo_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		error_message('Error upload app icon.');
                   		var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
                   	}
                    
                    
                    
                }
            });
        })();
    </script>
   

	<!-- end image upload ajax -->