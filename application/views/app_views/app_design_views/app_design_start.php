
	<script>
		    var base_url = '<?php echo site_url('app_controllers/app_design_start_controller/saveAppDesignStart');?>';
		    
		    $(document).ready(function(){});
			
			
			function preview_image(){
				
			    // get selected file element
			    var oFile = document.getElementById('app_icon').files[0];
			
			    // get preview element
			    var oImage = document.getElementById('app_icon_pre');
			
			    // prepare HTML5 FileReader
			    var oReader = new FileReader();
			        oReader.onload = function(e){
			
			        // e.target.result contains the DataURL which we will use as a source of the image
			        oImage.src = e.target.result;
			
			        oImage.onload = function () { // binding onload event
			
			            // we are going to display some custom image information here
			            
			        };
			    };

			    // read selected file as DataURL
			    oReader.readAsDataURL(oFile);
				
			}
		
		
		    function saveAppDesignState(){
		    	
				var data = $('form#from_app_design_start').serialize();
		        $('form#from_app_design_start').unbind('submit');                
		    	$.ajax({
		        	'url' : base_url,
		            'type' : 'POST', //the way you want to send data to your URL
		            'data' : data,
		            success: function(data) {
		            	
		            
		           		if(data == 1){
		    				window.location='<?php echo site_url("app_controllers/App_Design_Controller")?>'
		           		}
		           		
		           		else if(data == -1){
		    				error_message('App name is allready exists.');
		           		}
		           		
		           		else if(data == -2){
		    				error_message('Please upload app icon.');
		           		}
		           		
		           		
		           		
		           	
		            },
		       	});
		       	return false;
		   	}
		   	
	</script>
	<h1><i class="icon-reorder"></i>Getting Start</h1>
	
	<div class="panel panel-primary">
	    <div class="panel-heading">
	        <h3 class="panel-title">Create New Project</h3>
	    </div>
	    <form id="from_app_design_start" method="post" onsubmit="return saveAppDesignState()" class="form-horizontal">
	    <div class="panel-body">
	        	<div class="row">
			        	<div class="col-md-8" style="">
			        	
			        			<div class="form-group" id="div_app_name_container">
                                            <label for="textfield" class="col-md-4 control-label">App name : </label>
                                            <div class="input-group col-md-4 "> 
                                                    <input class="form-control" type="text"  name="txt_appname"  placeholder="type app name" name="txt_appname" id="txt_appname" required="true"/>                                                                             
                                            </div>
                                </div>
			        			
			        			<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">App description : </label>
                                            <div class="input-group col-md-4"> 
                                                    <textarea class="form-control" placeholder="type app desscription" name="txt_app_description" id="txt_app_description" rows="10" required="true"></textarea>                                                                             
                                            </div>
                                </div>
                                
                                <div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">App Icon : </label>
                                            <div class="input-group col-md-4"> 
                                            	                                                      
                                            	<a href="javascript:$('#myModal').modal('show')"><img   id="app_icon_pre" style="background-color:#fff;display:block;width:60px;height: 60px" 
                                            	src="<?php echo base_url('assets/site/images/no_image.png')?>"/></a>
                                            </div>
                                            
                                           	
                                </div>
			              
								<div class="form-group" id="button" align="center">
											<input type="submit" class="btn btn-primary" name='submit_createcardlot' id='submit_createcardlot' value="Create"/>
                                         
											<button type="button" class="btn" id="btn_next" type="button" class="sucess large" 
								        	onclick="">Cancel</button>
								        	
								        	
                                </div>
								
		
								
			            
			            </div>
			            
			            <div class="col-md-4"> <img src="<?php echo base_url('assets/site/images/e3x.jpg')?>" width="400" height="300" style="float: right"/></div>
		            </div>
	            
	    </div>
	   
	    </form>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form action="<?php echo site_url('app_controllers/app_design_start_controller/doUploadAppIcon')?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Upload App Icon</h4>
		      </div>
	      
		      <div class="modal-body" >
		      	
			      	<div class="form-group" id="div_app_description_container">
		                 <label for="textfield" class="col-md-4 control-label">Icon : </label>
		                 <div class="input-group col-md-6"> 
		                      <input type="file" id="app_icon" name="app_icon" onchange="preview_image()" class="form-control"/>                      	                                                      
		                    
		                 </div>
		                
		                    
		            </div>
		            <div class="form-group" id="" align="center">
		                 
		                 <div class="progress "  style="width: 500px">
  						 <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="">
						    0%
						 </div>
						 </div>
		                    
		            </div>
			        
			 </div>
	     	
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Upload</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
	<!-- img upload ajax -->
	<script src="<?php echo base_url('assets/site/js/jquery.js')?>"></script>
	<script src="<?php echo base_url('assets/site/js/jquery.form.js')?>"></script>
	
    <script>
        (function() {
 
            var progress_bar = $('.progress-bar');
 
            $('form').ajaxForm({
                beforeSend: function() {
                    //status.empty();
                    var percentVal = '0%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                success: function() {
                    var percentVal = '100%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                complete: function(xhr) {
                    //status.html(xhr.responseText);
                    $('#myModal').modal('hide');
                   
                   	if(xhr.responseText == '1'){
                   		sucess_message('Sucess fully upload icon');
                   	}
                   	else{
                   		
                   		document.getElementById('app_icon_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		error_message('Error upload app icon.');
                   		var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
                   	}
                    
                    
                    
                }
            });
        })();
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-43091346-1', 'devzone.co.in');
  ga('send', 'pageview');
 
</script>

	<!-- end image upload ajax -->

	