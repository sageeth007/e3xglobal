<script>
	
	
	function appBuild(){
		$.ajax({
		        	'url' : '<?php echo site_url('app_controllers/App_Build_Controller/buildApp');?>',
		            'type' : 'POST', 
		            'data' : {},
		            success: function(data) {
		            	if(data == 1){
							$('#buildAppModal').modal('hide');
		            		sucess_message('Sucess Build Your App....');
							window.location = '<?php echo site_url('app_controllers/App_Build_Controller/downloadFile');?>';
						}
		            	
		            },
		});
	}
</script>

<!-- Large modal -->
<div id="buildAppModal" data-backdrop="static" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header btn-success" style="">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h4 class="modal-title" id="myModalLabel">Sucess</h4>
		</div>
		
		<div class="modal-body" >
			
			<div align="center">
				<img src="<?php echo base_url('images/agt_action_success.png')?>" />
		        <b>Congratulations , Building your app .....</b>
			</div>
			</br>
			<div align="center">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-success" disabled="disabled" onclick="appBuild()">Download</button>
			</div>
			
		</div>
		
		
    </div>
  </div>
</div>



