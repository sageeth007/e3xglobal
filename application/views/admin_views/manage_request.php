<h1><i class="icon-reorder"></i>
	Service Request
</h1>
	
<div class="panel panel-primary">
	    <div class="panel-heading">
	        <h3 class="panel-title">Manage Service Request</h3>
	    </div>
	    
	    <div class="panel-body">
	        	<div class="row" align="center"> 
			        			
							        			<table class="table table-hover" style="width: 1080px">
													<thead>
														<tr>
																<th style="width: 200px" class="text-left">Org Name</th>
									                            <th class="text-left">Email </th>
									                            <th style="width: 200px" class="text-left">TP Number</th>
									                       <!--      <th class="text-left">Chairman</th> -->
									                       <!--     <th class="text-left">Web Site</th> -->
									                            <th style="width: 350px" class="text-left">Address Line 1</th>
									                            <th style="width: 350px" class="text-left">Address Line 2</th>
									                            <th class="text-left">City</th>
									                            <th class="text-left">State </th>
									                       <!--     <th style="width: 200px" class="text-left">Zip Code</th> -->
									                            <th class="text-left">Org Type</th>
									                            <th  style="width: 350px" class="text-left">Action</th>														
														</tr>								
													</thead>								
													<tbody>								
														<?php foreach ($request->result() as $row) { ?>	
														<tr>
															<td class="right"><?php echo $row->OrganizationName; ?></td>
															<td class="right"><?php echo $row->Email; ?></td>
															<td class="right"><?php echo $row->ContactNumber; ?></td>
														<!--	<td class="right"><?php echo $row->Chairman; ?></td> -->
														<!--	<td class="right"><?php echo $row->WebSite; ?></td> -->
														<!--	<td class="right"><?php echo $row->BuildingNumber; ?></td> -->
															<td class="right"><?php echo $row->Street; ?></td>
															<td class="right"><?php echo $row->AddressLine2; ?></td>
															<td class="right"><?php echo $row->City; ?></td>
															<td class="right"><?php echo $row->State; ?></td>
														<!--	<td class="right"><?php echo $row->ZipCode; ?></td> -->
															<td class="right"><?php echo $row->OrganizationType; ?></td>
															
															<td  class="right">
																<a href="<?php echo site_url('admin_controllers/manage_request_controller/loadApprovalRequest'); ?>/<?php echo $row->OrganizationId; ?>" class="btn btn-sm btn-success" >
																	<span class="glyphicon glyphicon-pencil"></span></a>
																
																<a href="<?php echo site_url('admin_controllers/manage_request_controller/rejectRequest'); ?>/<?php echo $row->OrganizationId; ?>" 
																	class="btn btn-sm btn-danger ask-delete" >
																	<span class="glyphicon glyphicon-remove"></span></a>
																
															</td>
														
															</tr>
													
													<?php } ?>
										
										
										
													</tbody>
								</table>	
			        	
			        			
			              
		
		            </div>
		            
		            
	            
	    </div>
	   
	  
</div>


