<script>
		    var base_url = '<?php echo site_url('Main_Home_Controller/loginUser');?>';
		    
		    $(document).ready(function(){});
		
		    function login(){
		    	
		    	
				var data = $('form#from_login').serialize();
		        $('form#from_login').unbind('submit');                
		    	$.ajax({
		        	'url' : base_url,
		            'type' : 'POST', //the way you want to send data to your URL
		            'data' : data,
		            success: function(data) {
		            	
		          
		     
		            	
		           		if(data == 1){
		           			
		           			
		           			$("#user_id_container").removeClass("error-state");
		           			$("#password_container").removeClass("error-state");
		           			
		           			document.getElementById("span_popup_sucess").innerHTML = "Sucessfully login <?php echo $this->session->userdata('user_id') ?>";
							$( "#dialog_sucess" ).dialog({
		      				modal: true,
		      				buttons: {Ok: function() {$( this ).dialog( "close" );}}
		    				});
		    				
		    				window.location = "<?php echo  site_url($this->session->userdata('default_page')) ?>";
		    				
		    				
		           		}
		           		else if(data == -1){
		           			$("#password_container").removeClass("error-state");
							$("#user_id_container").addClass("error-state");
							
							document.getElementById("span_popup_error").innerHTML ="Error.<br><b>Please fill userid</b>";
							$( "#dialog_error" ).dialog({
		      				modal: true,
		      				buttons: {Ok: function() {$( this ).dialog( "close" );}}
		    				});
							
		           		}
		           		else if(data == -2){
		           			$("#user_id_container").removeClass("error-state");
		           			$("#password_container").addClass("error-state");
		           			
		           			document.getElementById("span_popup_error").innerHTML ="Error.<br><b>Please fill password</b>";
							$( "#dialog_error" ).dialog({
		      				modal: true,
		      				buttons: {Ok: function() {$( this ).dialog( "close" );}}
		    				});
		           			
		           		}
		           		else if(data == -3){
		           			$("#user_id_container").addClass("error-state");
		           			$("#password_container").addClass("error-state");
		           			
		           			document.getElementById("span_popup_error").innerHTML ="Error.<br><b>Please fill both fields</b>";
							$( "#dialog_error" ).dialog({
		      				modal: true,
		      				buttons: {Ok: function() {$( this ).dialog( "close" );}}
		    				});
		           		}
		           		else if(data == -4){
		           		
		           			document.getElementById("span_popup_error").innerHTML ="Error.<br><b>Please check userid and password</b>";
							$( "#dialog_error" ).dialog({
		      				modal: true,
		      				buttons: {Ok: function() {$( this ).dialog( "close" );}}
		    				});
		           		}
		           		
		           		
		           	
		            },
		       	});
		       	return false;
		   	}
		   	
	</script>

<div  style="height: 88%; width: 100%; 
     background-repeat: no-repeat;/*we want to have one single image not a repeated one*/
     background-size: cover;/*this sets the image to fullscreen covering the whole screen*/
      background-position: 0% 0%; " >
      <div id="child" style="position:absolute;top:90px;left:50px ">
    <img  src="<?php echo base_url('assets/site/images/logonew_alone.png')?>">
    </div>
    
</div>

<div class="panel" style="position:absolute;top:100px;left:700px ">
    <div class="panel-header" style="height:50px;width: 500px;text-align:center;color:#000000;font-weight:bold">
        Welcome to E3xGlobal 
    </div>
    <div class="panel-content" style="background-color:#EBEBEB">
         <div style="background-color:#FFFFFF">
                                <form id="from_login" method="post" style="background-color:#EBEBEB" onsubmit="return login()">
                                    <fieldset>
                                        <legend>Login Here</legend>
                                        <label>User Name</label>
                                        <div class="input-control text" data-role="input-control" id="user_id_container">
                                            <input type="text" placeholder="Type User Name" name="userid" id="userid">
                                            <button class="btn-clear" tabindex="-1"></button>
                                        </div>
                                        <label>Password</label>
                                        <div class="input-control password" data-role="input-control" id="password_container">
                                            <input type="password" placeholder="Type Password" name="password" id="password" autofocus>
                                            <button class="btn-reveal" tabindex="-1"></button>
                                        </div>
                                        
                                        <input type="submit" value="Submit" class="primary large">
                                    </fieldset>
                                </form>
                            </div>
                           
    </div>
</div>
