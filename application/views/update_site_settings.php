<h3><i class="icon-reorder"></i>Site Settings</h3>

<ul class="nav nav-tabs">
    <li class="active"><a href="#">Contact Detail Settings</a></li>
    <li ><a href="<?php echo site_url('site_settings/load_site_configuration_details') ?>">Site Configuration</a></li>
   
</ul>
<br/>

<div class="row">
    <div class="col-md-12">
        <form  method="POST" class='form-horizontal' enctype="multipart/form-data"    
               action="<?php echo site_url('Site_Settings/add_or_update_contact_details') ?>">
            
   			<?php
            if (!empty($site_setting)) {
                foreach ($site_setting->result() as $row) {
                    if ($row->Key == 'site_address') {
                        $site_address = $row->Value;
                    } else if ($row->Key == 'map_address') {
                        $map_address = $row->Value;
                    } else if ($row->Key == 'contact_no') {
                        $contact_no = $row->Value;
                    } else if ($row->Key == 'fax_no') {
                        $fax_no = $row->Value;
                    } else if ($row->Key == 'email') {
                        $email = $row->Value;
                    }
                }
            }
            ?> 
            
            <div class="form-group">
                <label for="textfield" class="col-md-4 control-label">Address : </label>
                <div class="input-group col-md-4"> 
                    <input type="text"  name="site_address" 
                           value="<?php echo!empty($site_address) ? $site_address : set_value('site_address'); ?>" required class="form-control"/>                                                                             
                </div>
            </div>
			
            <div class="form-group">
                <label for="driver_name" class="col-md-4 control-label">Map Address : </label>
                <div class="input-group col-md-4"> 
                    <input type="text"  name="map_address" 
                           value="<?php echo!empty($map_address) ? $map_address : set_value('map_address'); ?>" required class="form-control"/>                                                                             
                </div>
            </div>

            <div class="form-group">
                <label for="textfield" class="col-md-4 control-label">Contact Numbers : </label>
                <div class="input-group col-md-4"> 
                    <input type="text"  name="contact_no" 
                           Value="<?php echo!empty($contact_no) ? $contact_no : set_value('contact_no'); ?>" required class="form-control"/>                                                                             
                </div>
            </div>

            <div class="form-group">
                <label for="textfield" class="col-md-4 control-label">Fax Numbers : </label>
                <div class="input-group col-md-4"> 
                    <input type="text"  name="fax_no" 
                           Value="<?php echo!empty($fax_no) ? $fax_no : set_value('fax_no'); ?>" required class="form-control"/>                                                                             
                </div>
            </div>

            <div class="form-group">
                <label for="textfield" class="col-md-4 control-label">Email : </label>
                <div class="input-group col-md-4"> 
                    <input type="text"  name="email" 
                           Value="<?php echo!empty($email) ? $email : set_value('email'); ?>" required class="form-control"/>                                                                             
                </div>
            </div>

			

            <div class="form-group" id="button" align="center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn" onclick="window.location = '<?php echo site_url('site_setting') ?>'">Cancel</button>
            </div> 
        </form>
    </div>
</div>