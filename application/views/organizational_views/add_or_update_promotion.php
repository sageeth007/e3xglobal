	<script>
		function preview_image(){
				
			    // get selected file element
			    var oFile = document.getElementById('content_label').files[0];
			
			    // get preview element
			    var oImage = document.getElementById('image_pre');
			
			    // prepare HTML5 FileReader
			    var oReader = new FileReader();
			        oReader.onload = function(e){
			
			        // e.target.result contains the DataURL which we will use as a source of the image
			        oImage.src = e.target.result;
			
			        oImage.onload = function () { // binding onload event
			
			            // we are going to display some custom image information here
			            
			        };
			    };

			    // read selected file as DataURL
			    oReader.readAsDataURL(oFile);
				
			}
	
	
	</script>
	
	
	<h1><i class="icon-reorder"></i>Promotion </h1>
	
	<div class="panel panel-primary">
	    <div class="panel-heading">
	        <h3 class="panel-title">Create New Promotion</h3>
	    </div>
	    <form id="from_app_design_start" method="post"  class="form-horizontal" 
	    action=
		"<?php echo site_url('organization_controllers/distributed_organization_controller/addPromotion') ?>/<?php echo !empty($promotion->PromId) ? $promotion->PromId: "";?>">
	    <div class="panel-body">
	        	<div class="row">
			        	
								<input type="hidden" name="BranchId" value="<?php echo $branch['branch_id'] ?>" />
			        			<div class="form-group" id="div_app_name_container">
                                            <label for="textfield" class="col-md-4 control-label">Promotion Title : </label>
                                            <div class="input-group col-md-4 "> 
                                                    <input 
													value="<?php echo !empty($promotion->Title)?$promotion->Title:'' ?>" 
													class="form-control" 
													type="text"    
													placeholder="content title" 
													name="Title" 
													id="Title" 
													required="true"
													/>                                                                             
													
											</div>
                                </div>
			        			
			        			<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">Content : </label>
                                            <div class="input-group col-md-4"> 
                                                    <textarea 
													class="form-control" 
													placeholder="content body" 
													name="Content" id="Content" 
													rows="10" 
													required="true">
														<?php echo !empty($promotion->Content)?$promotion->Content:'' ?>
													</textarea>                                                                             
                                            </div>
                                </div>
                                
								<div class="form-group" id="div_app_description_container">
                                            <label for="textfield" class="col-md-4 control-label">Image : </label>
                                            <div class="input-group col-md-4"> 
                                            	                                                      
                                            	<a href="javascript:$('#myModal').modal('show')">
												<img   
													id="image_pre" 
													name="image_pre"
													style="background-color:#fff;display:block;width:60px;height: 60px" 
													src="<?php echo !empty($promotion->ImagePath)?base_url($promotion->ImagePath):base_url('assets/site/images/no_image.png') ?>"
													/>
												
												</a>
                                            </div>
                                            
                                           	
                                </div>
                                
			              
								<div class="form-group" id="button" align="center">
											<input type="submit" class="btn btn-primary" name='submit_text' id='submit_text' value="Submit"/>
                                         
											<button type="button" class="btn" id="btn_next" type="button" class="sucess large" 
								        	onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadManagePromations')?>/<?php echo $branch['branch_id']?>'">Back</button>
								        	
								        	
                                </div>
	
			            
			           
		            </div>
	            
	    </div>
	   
	    </form>
	</div>
	
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form id="form_upload_label" action="<?php echo site_url('organization_controllers/distributed_organization_controller/doUploadContentLabel')?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Upload Image</h4>
		      </div>
	      
		      <div class="modal-body" >
		      	
			      	<div class="form-group" id="div_app_description_container">
		                 <label for="textfield" class="col-md-4 control-label">Image : </label>
		                 <div class="input-group col-md-6"> 
		                      <input type="file" id="content_label" name="content_label" onchange="preview_image()" class="form-control"/>                      	                                                      
		                    
		                 </div>
		                
		                    
		            </div>
		            <div class="form-group" id="" align="center">
		                 
		                 <div class="progress "  style="width: 500px">
  						 <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="">
						    0%
						 </div>
						 </div>
		                    
		            </div>
			        
			 </div>
	     	
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Upload</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
	<!-- img upload ajax -->
	<script src="<?php echo base_url('assets/site/js/jquery.js')?>"></script>
	<script src="<?php echo base_url('assets/site/js/jquery.form.js')?>"></script>
	
    <script>
        (function() {
 
            var progress_bar = $('.progress-bar');
 
            $('#form_upload_label').ajaxForm({
                beforeSend: function() {
                    //status.empty();
                    var percentVal = '0%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                success: function() {
                    var percentVal = '100%';
                    progress_bar.width(percentVal)
                    progress_bar.html(percentVal);
                },
                complete: function(xhr) {
                    //status.html(xhr.responseText);
                    $('#myModal').modal('hide');
                   
                   	if(xhr.responseText == '1'){
                   		sucess_message('Sucess fully upload icon');
                   	}
                   	else{
                   		
                   		document.getElementById('image_pre').src = "<?php echo base_url('assets/site/images/no_image.png')?>";
                   		error_message('Error upload app icon.');
                   		var percentVal = '0%';
                    	progress_bar.width(percentVal)
                    	progress_bar.html(percentVal);
                   	}
                    
                    
                    
                }
            });
        })();
    </script>
    

	<!-- end image upload ajax -->
	
	
	     	
	


	