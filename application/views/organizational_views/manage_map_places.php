<h1><i class="icon-reorder"></i>Map Places
	<div class="row" style="float: right">
			<input value="Add new place"  type="button" class="btn btn-primary btn-sm"
				onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadAddNewMapPlace')?>'"/>
	
	</div>
</h1>
	
	<div class="panel panel-primary">
	    <div class="panel-heading">
	        <h3 class="panel-title">Manage Map Place</h3>
	    </div>
	    
	    
	    
	    <form id="from_app_design_start" method="post" onsubmit="return saveAppDesignState()" class="form-horizontal">
	    <div class="panel-body">
	        	<div class="row" align="center"> 
			        			<?php 
												$is_have_place = $map_place->num_rows();
												if($map_place->num_rows() > 0){
								?>
							        			<table class="table table-hover" style="width: 800px">
													<thead>
														<tr>
																<th style="width: 150px;">Branch Address</th>							
																<th style="width: 150px;">Branch Name</th>
																<th style="width: 150px;">Branch latitude </th>
																<th style="width: 150px;">Branch longitude </th>
																<th  style="width: 150px;">Action</th>															
														</tr>								
													</thead>								
													<tbody>								
										
										
										<?php		
														foreach ($map_place->result() as $row) { ?>	
														
														<tr>
															<td ><?php echo $row->BranchAddress; ?></td>
															<td ><?php echo $row->BranchName; ?></td>
															<td ><?php echo $row->Branch_Lat; ?></td>
															<td ><?php echo $row->Branch_Lng; ?></td>
															<td >
																<a href="#" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
																<a href="<?php echo site_url('organization_controllers/distributed_organization_controller/loadManagePromations') ?>/<?php echo $row->BranchId?>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-arrow-right"></span></a>
																<a href="#" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
															</td>
														</tr>
													
										<?php }}
												 
												else{
										?>
													<div style="width: 200px" class="alert alert-warning">No Places Aviable</div>
										<?php		
												}		
												
										 ?>
										
									</tbody>
								</table>	
			        	
			        			
			              
		
		            </div>
		            
		            
	            
	    </div>
	   
	    </form>
	    
</div>


<div class="form-group" id="button" align="center" >
		<button style="" onclick="$('#buildAppModal').modal('show')" class="btn btn-success "  >
		<span class=""></span>Build App</button>
									  
		<button id="btn_next" onclick="window.location='<?php echo site_url('app_controllers/app_content_controller/loadTextContents') ?>'" class="btn btn-default "   >
	    <span class=""></span>Back</button>
									  
</div>
			
	
	
	
	
