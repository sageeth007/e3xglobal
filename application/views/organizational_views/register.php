<script>
	
	
   	
   	function putText(){
   		document.getElementById("txtOrganization").value= "My company";
   		document.getElementById("txtChairman").value= "Tharanga";
   		document.getElementById("txtEmail").value= "someone@somthing.com";
   		document.getElementById("Website").value= "company.com";
   		document.getElementById("txtTpNumber").value= "0112248736";
   		document.getElementById("txtBuildingNum").value= "432/1";
   		document.getElementById("txtStreet").value= "Malabe";
   		document.getElementById("txtAddressL2").value= "Kaduwela";
   		document.getElementById("txtCity").value= "Colombo";
   		document.getElementById("txtState").value= "Western";
   		document.getElementById("Zip").value= "71500";
   	}
   	
   	function clearText(){
   		document.getElementById("txtOrganization").value= "";
   		document.getElementById("txtChairman").value= "";
   		document.getElementById("txtEmail").value= "";
   		document.getElementById("Website").value= "";
   		document.getElementById("txtTpNumber").value= "";
   		document.getElementById("txtBuildingNum").value= "";
   		document.getElementById("txtStreet").value= "";
   		document.getElementById("txtAddressL2").value= "";
   		document.getElementById("txtCity").value= "";
   		document.getElementById("txtState").value= "";
   		document.getElementById("Zip").value= "";
   	}
</script>


		<div class="panel">
                <div class="panel-header">
                <h1>
                    Register
                </h1>
				</div>
				
				<div class="panel-content">
					
						<div class="span9">
							<br/>
							<div >
								<form action="<?php echo site_url('organization_controllers/request_controller/registerController')?>" method="post" >
									
									<fieldset>
										<legend>Organization's info</legend>
										<label>Organization Name</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Organization" name="organization" id="txtOrganization">
											<button class="btn-clear" tabindex="-1"></button>
										</div>						
										
										<label>Chairman</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Chairman's Name" name="chairman" id="txtChairman">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Email</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="someone@company.com" name="email" id="txtEmail">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Website</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="company.com" name="website" id="Website">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<legend>Contact Details</legend>
										<label>Contact Number</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Company's phone number" name="tpNumber" id="txtTpNumber">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Building Number</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Building number" name="buildingNumber" id="txtBuildingNum">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Street</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Street name" name="street" id="txtStreet">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Address Line 2</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Address Line 2" name="adrsLine2" id="txtAddressL2">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>City</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="City" name="city" id="txtCity">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>State</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="State" name="state" id="txtState">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<label>Zip Code</label>
										<div class="input-control text" data-role="input-control">
											<input type="text" placeholder="Zip code" name="zipCode" id="Zip">
											<button class="btn-clear" tabindex="-1"></button>
										</div>
										
										<!-- Script by hscripts.com -->
										<label>Country</label>
										<select name="country">
											<option value="Afghanistan">Afghanistan</option>
											<option value="Albania">Albania</option>
											<option value="Algeria">Algeria</option>
											<option value="Andorra">Andorra</option>
											<option value="Angola">Angola</option>
											<option value="Antarctica">Antarctica</option>
											<option value="Antigua and Barbuda">Antigua and Barbuda</option>
											<option value="Argentina">Argentina</option>
											<option value="Armenia">Armenia</option>
											<option value="Australia">Australia</option>
											<option value="Austria">Austria</option>
											<option value="Azerbaijan">Azerbaijan</option>
											<option value="Bahamas">Bahamas</option>
											<option value="Bahrain">Bahrain</option>
											<option value="Bangladesh">Bangladesh</option>
											<option value="Barbados">Barbados</option>
											<option value="Belarus">Belarus</option>
											<option value="Belgium">Belgium</option>
											<option value="Belize">Belize</option>
											<option value="Benin">Benin</option>
											<option value="Bermuda">Bermuda</option>
											<option value="Bhutan">Bhutan</option>
											<option value="Bolivia">Bolivia</option>
											<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
											<option value="Botswana">Botswana</option>
											<option value="Brazil">Brazil</option>
											<option value="Brunei">Brunei</option>
											<option value="Bulgaria">Bulgaria</option>
											<option value="Burkina Faso">Burkina Faso</option>
											<option value="Burma">Burma</option>
											<option value="Burundi">Burundi</option>
											<option value="Cambodia">Cambodia</option>
											<option value="Cameroon">Cameroon</option>
											<option value="Canada">Canada</option>
											<option value="Cape Verde">Cape Verde</option>
											<option value="Central African Republic">Central African Republic</option>
											<option value="Chad">Chad</option>
											<option value="Chile">Chile</option>
											<option value="China">China</option>
											<option value="Colombia">Colombia</option>
											<option value="Comoros">Comoros</option>
											<option value="Congo, Democratic Republic">Congo, Democratic Republic</option>
											<option value="Congo, Republic of the">Congo, Republic of the</option>
											<option value="Costa Rica">Costa Rica</option>
											<option value="Cote d'Ivoire">Cote d'Ivoire</option>
											<option value="Croatia">Croatia</option>
											<option value="Cuba">Cuba</option>
											<option value="Cyprus">Cyprus</option>
											<option value="Czech Republic">Czech Republic</option>
											<option value="Denmark">Denmark</option>
											<option value="Djibouti">Djibouti</option>
											<option value="Dominica">Dominica</option>
											<option value="Dominican Republic">Dominican Republic</option>
											<option value="East Timor">East Timor</option>
											<option value="Ecuador">Ecuador</option>
											<option value="Egypt">Egypt</option>
											<option value="El Salvador">El Salvador</option>
											<option value="Equatorial Guinea">Equatorial Guinea</option>
											<option value="Eritrea">Eritrea</option>
											<option value="Estonia">Estonia</option>
											<option value="Ethiopia">Ethiopia</option>
											<option value="Fiji">Fiji</option>
											<option value="Finland">Finland</option>
											<option value="France">France</option>
											<option value="Gabon">Gabon</option>
											<option value="Gambia">Gambia</option>
											<option value="Georgia">Georgia</option>
											<option value="Germany">Germany</option>
											<option value="Ghana">Ghana</option>
											<option value="Greece">Greece</option>
											<option value="Greenland">Greenland</option>
											<option value="Grenada">Grenada</option>
											<option value="Guatemala">Guatemala</option>
											<option value="Guinea">Guinea</option>
											<option value="Guinea-Bissau">Guinea-Bissau</option>
											<option value="Guyana">Guyana</option>
											<option value="Haiti">Haiti</option>
											<option value="Honduras">Honduras</option>
											<option value="Hong Kong">Hong Kong</option>
											<option value="Hungary">Hungary</option>
											<option value="Iceland">Iceland</option>
											<option value="India">India</option>
											<option value="Indonesia">Indonesia</option>
											<option value="Iran">Iran</option>
											<option value="Iraq">Iraq</option>
											<option value="Ireland">Ireland</option>
											<option value="Israel">Israel</option>
											<option value="Italy">Italy</option>
											<option value="Jamaica">Jamaica</option>
											<option value="Japan">Japan</option>
											<option value="Jordan">Jordan</option>
											<option value="Kazakhstan">Kazakhstan</option>
											<option value="Kenya">Kenya</option>
											<option value="Kiribati">Kiribati</option>
											<option value="Korea, North">Korea, North</option>
											<option value="Korea, South">Korea, South</option>
											<option value="Kuwait">Kuwait</option>
											<option value="Kyrgyzstan">Kyrgyzstan</option>
											<option value="Laos">Laos</option>
											<option value="Latvia">Latvia</option>
											<option value="Lebanon">Lebanon</option>
											<option value="Lesotho">Lesotho</option>
											<option value="Liberia">Liberia</option>
											<option value="Libya">Libya</option>
											<option value="Liechtenstein">Liechtenstein</option>
											<option value="Lithuania">Lithuania</option>
											<option value="Luxembourg">Luxembourg</option>
											<option value="Macedonia">Macedonia</option>
											<option value="Madagascar">Madagascar</option>
											<option value="Malawi">Malawi</option>
											<option value="Malaysia">Malaysia</option>
											<option value="Maldives">Maldives</option>
											<option value="Mali">Mali</option>
											<option value="Malta">Malta</option>
											<option value="Marshall Islands">Marshall Islands</option>
											<option value="Mauritania">Mauritania</option>
											<option value="Mauritius">Mauritius</option>
											<option value="Mexico">Mexico</option>
											<option value="Micronesia">Micronesia</option>
											<option value="Moldova">Moldova</option>
											<option value="Mongolia">Mongolia</option>
											<option value="Morocco">Morocco</option>
											<option value="Monaco">Monaco</option>
											<option value="Mozambique">Mozambique</option>
											<option value="Namibia">Namibia</option>
											<option value="Nauru">Nauru</option>
											<option value="Nepal">Nepal</option>
											<option value="Netherlands">Netherlands</option>
											<option value="New Zealand">New Zealand</option>
											<option value="Nicaragua">Nicaragua</option>
											<option value="Niger">Niger</option>
											<option value="Nigeria">Nigeria</option>
											<option value="Norway">Norway</option>
											<option value="Oman">Oman</option>
											<option value="Pakistan">Pakistan</option>
											<option value="Panama">Panama</option>
											<option value="Papua New Guinea">Papua New Guinea</option>
											<option value="Paraguay">Paraguay</option>
											<option value="Peru">Peru</option>
											<option value="Philippines">Philippines</option>
											<option value="Poland">Poland</option>
											<option value="Portugal">Portugal</option>
											<option value="Qatar">Qatar</option>
											<option value="Romania">Romania</option>
											<option value="Russia">Russia</option>
											<option value="Rwanda">Rwanda</option>
											<option value="Samoa">Samoa</option>
											<option value="San Marino">San Marino</option>
											<option value=" Sao Tome"> Sao Tome</option>
											<option value="Saudi Arabia">Saudi Arabia</option>
											<option value="Senegal">Senegal</option>
											<option value="Serbia and Montenegro">Serbia and Montenegro</option>
											<option value="Seychelles">Seychelles</option>
											<option value="Sierra Leone">Sierra Leone</option>
											<option value="Singapore">Singapore</option>
											<option value="Slovakia">Slovakia</option>
											<option value="Slovenia">Slovenia</option>
											<option value="Solomon Islands">Solomon Islands</option>
											<option value="Somalia">Somalia</option>
											<option value="South Africa">South Africa</option>
											<option value="Spain">Spain</option>
											<option value="Sri Lanka">Sri Lanka</option>
											<option value="Sudan">Sudan</option>
											<option value="Suriname">Suriname</option>
											<option value="Swaziland">Swaziland</option>
											<option value="Sweden">Sweden</option>
											<option value="Switzerland">Switzerland</option>
											<option value="Syria">Syria</option>
											<option value="Taiwan">Taiwan</option>
											<option value="Tajikistan">Tajikistan</option>
											<option value="Tanzania">Tanzania</option>
											<option value="Thailand">Thailand</option>
											<option value="Togo">Togo</option>
											<option value="Tonga">Tonga</option>
											<option value="Trinidad and Tobago">Trinidad and Tobago</option>
											<option value="Tunisia">Tunisia</option>
											<option value="Turkey">Turkey</option>
											<option value="Turkmenistan">Turkmenistan</option>
											<option value="Uganda">Uganda</option>
											<option value="Ukraine">Ukraine</option>
											<option value="United Arab Emirates">United Arab Emirates</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="United States">United States</option>
											<option value="Uruguay">Uruguay</option>
											<option value="Uzbekistan">Uzbekistan</option>
											<option value="Vanuatu">Vanuatu</option>
											<option value="Venezuela">Venezuela</option>
											<option value="Vietnam">Vietnam</option>
											<option value="Yemen">Yemen</option>
											<option value="Zambia">Zambia</option>
											<option value="Zimbabwe">Zimbabwe</option>
										</select>
										
										<!-- Script by hscripts.com -->   
										
										<div>
											<div class="input-control radio default-style" data-role="input-control">
												<br/>
												<label>Organization Type</label>
												<label>
													<input type="radio" name="orgType" checked value="Distributed"/>
													<span class="check"></span>
													Distributed
												</label>
											</div>
											
											<div class="input-control radio  default-style" data-role="input-control">
												<label>
													<input type="radio" name="orgType" value="Non Distributed"/>
													<span class="check"></span>
													Non Distributed
												</label>
											</div>
										</div>
										<br/>
                                        <input type="submit" class="primary large"  value="Submit" name="submit"/> 
                                        <input style="position: relative ;left:30px" type="button" value="Clear" onclick="clearText()" class="primary large"/>
                                        <input style="position:relative ;left:60px  " type="button" value="Demo" onclick="putText();" class="primary large"/>
                                        
									</fieldset>
								</form>
