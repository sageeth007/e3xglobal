<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(6.8940701, 79.90247790000001),
    zoom: 8
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
 

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  
    }
	
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    
    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
    
	document.getElementById('p_lat').value = place.geometry.location.lat();
	document.getElementById('p_lng').value = place.geometry.location.lng();
	document.getElementById('p_addr').value = address;
	document.getElementById('p_name').value = place.name;
	document.getElementById('submit_add_place').disabled=false;
	
    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
	
    infowindow.open(map, marker);
  });

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script>
		    var base_url = '<?php echo site_url('organization_controllers/distributed_organization_controller/addMapPlace');?>';
		    function addMapPlace(){
		    	
				var data = $('form#form_add_place').serialize();
		        $('form#form_add_place').unbind('submit');                
		    	$.ajax({
		        	'url' : base_url,
		            'type' : 'POST', //the way you want to send data to your URL
		            'data' : data,
					
		            success: function(data) {
		            
		           		if(data == 1){
		    				sucess_message('Sucessfully add new place');
		           		}
		            	
		           		else {
		    				error_message('Error adding new place.');
		           		}
		           		
		            },
		       	});
		       	return false;
		   	}
		   	
	</script>
<h1><i class="icon-reorder"></i>Add New Map Places
	
</h1>
<form id="form_add_place" method="post" action="" onsubmit="return addMapPlace()">
<div class="form-group" id="button" align="center">
	<input disabled="disabled" type="submit" class="btn btn-primary " name='submit_add_place' id='submit_add_place' value="Add New Place"/>
                                         
	<button type="button" class="btn" id="btn_next" type="button" class="sucess large" 
	onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadManageMapPlaces') ?>'">Back</button>
	<input type="hidden" name="p_lat" id="p_lat" />							        	
	<input type="hidden" name="p_lng" id="p_lng" />
	<input type="hidden" name="p_addr" id="p_addr" />
	<input type="hidden" name="p_name" id="p_name" />												        	
</div>
</form>
<div id="map_container"  align="center" style="width: 100%;height: 100%">
			<input id="pac-input"  type="text" class="form-control" style="width: 200px"
	        	placeholder="Enter a location" >
	    	
	    	<div id="map-canvas" style="width:1150px;height:450px;" ></div>	
	
	
</div>