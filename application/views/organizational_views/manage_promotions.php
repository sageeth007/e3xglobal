<h1><i class="icon-reorder"></i>Promotions
	<div class="row" style="float: right">
			<input value="Add new promotion"  type="button" class="btn btn-primary btn-sm"
				onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadAddNewPromotion')?>/<?php echo -1 ?>/<?php echo $branch->BranchId?>'"/>
			<button type="button" class="btn" id="btn_next" type="button" class="sucess large" 
				onclick="window.location='<?php echo site_url('organization_controllers/distributed_organization_controller/loadManageMapPlaces') ?>'">Back</button>
	</div>
	
</h1>
	
	<div class="panel panel-primary">
	    <div class="panel-heading">
	        <h3 class="panel-title">Manage Promotions : <b><?php echo  $branch->OrgId.','.$branch->BranchName ?></b></h3>
	    </div>
	    
	    
	    <form id="from_app_design_start" method="post" onsubmit="return saveAppDesignState()" class="form-horizontal">
	    <div class="panel-body">
	        	<div class="row" align="center"> 
			        			<?php 
						
												if($promotions->num_rows() > 0){
								?>
							        			<table class="table table-hover" style="width: 800px">
													<thead>
														<tr>
																<th style="width: 150px;">Title</th>							
																<th style="width: 150px;">Content</th>
																<th style="width: 150px;">Image Path</th>
																<th  style="width: 150px;">Action</th>															
														</tr>								
													</thead>								
													<tbody>								
										
										
										<?php		
														foreach ($promotions->result() as $row) { ?>	
														
														<tr>
															<td ><?php echo $row->Title; ?></td>
															<td ><?php echo $row->Content; ?></td>
															<td >
																<img   
																id="content_label_pre" 
																style="background-color:#fff;display:block;width:60px;height: 60px" 
																src="<?php echo  base_url($row->ImagePath); ?>"
																/>
															
															
															</td>
				
															<td >
																<a href="<?php echo site_url('organization_controllers/distributed_organization_controller/loadAddNewPromotion')?>/<?php echo $row->PromId?>/<?php echo $branch->BranchId?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
																
																<a href="#" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
															</td>
														</tr>
													
										<?php }}
												 
												else{
										?>
													<div style="width: 200px" class="alert alert-warning">No Promotions Aviable</div>
										<?php		
												}		
												
										 ?>
										
									</tbody>
								</table>	
			        	
			        			
			              
		
		            </div>
		            
		            
	            
	    </div>
	   
	    </form>
	    
</div>
			
	
	
	
	
