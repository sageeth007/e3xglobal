<?php
class General_DB extends CI_Model {
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    

    function insert($table_name, $data) {
        $query = $this->db->insert($table_name, $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insertGetId($table_name, $data){
        $query = $this->db->insert($table_name, $data);
        $insert_id = $this->db->insert_id();
        if ($query) {
            return $insert_id;
        } else {
            return FALSE;
        }
    }

    
    function update($table_name, $table_field, $id, $data) {
        $this->db->where($table_field, $id);
        $update_data = $this->db->update($table_name, $data);
        if ($update_data == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function delete($table, $table_field, $id) {
        $delete = $this->db->delete($table, array($table_field => $id));
        if ($delete) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getWhere($table, $where = null, $order_by_field = null, $order_by='desc', $limit, $offset) {
           if($where){ $this->db->where($where); }
           if($order_by_field){ $this->db->order_by($order_by_field, $order_by); } 

            $query = $this->db->get($table, $limit, $offset);

            if ($query) {
                return $query;
            } else {
                return FALSE;
            }
        }

    function id_to_dbrow($table, $table_id_field, $id){
        $query = $this->db->get_where($table, array($table_id_field => $id));
        if ($query->num_rows() > 0)
        {
           $result = $query->row(); 
            return $result;
        } else {
            return FALSE;
        }
    }
	
	function select_all($table){
		$query=$this->db->get($table);
		return $query;
	}
	
	function select_column($table,$table_id_field)
	{
		foreach($table_id_field as $col)
			$this->db->select($col);
		
		$query = $this->db->get($table);
	}
	
	function username_available($table,$table_id_field,$id)
	{
		$query = $this->db->get_where($table, array($table_id_field => $id));
		return $query->num_rows();
		
	}
	
	function check_login($table,$username,$password)
	{
		$query = $this->db->get_where($table, array('emp_login_email' => $username),array('emp_login_password' => $password));
		//if($query->num_rows()>0)
			//return TRUE;
		//else
			return $query->num_rows();
	}

}