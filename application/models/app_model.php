<?php
    class App_Model extends CI_Model{
    	
		public function checkAppName($app_name){
				
			$query = $this->db->query("SELECT * FROM app_designs WHERE AppName ='".$app_name."'");
			if ($query->num_rows() > 0)
        	{
            	return -1;
        	} 
        	else 
        	{
            	return 1;
        	}
		}
		
		public function saveAppDesign($data){
			$query = $this->db->insert("app_designs", $data);
	        if ($query) {
	            return 1;
	        }
			else 
			{
	            return -1;
	        }
		}
		
		function id_to_dbrow($table, $table_id_field, $id){
	        $query = $this->db->get_where($table, array($table_id_field => $id));
	        if ($query->num_rows() > 0)
	        {
	           $result = $query->row(); 
	            return $result;
	        } else {
	            return FALSE;
	        }
    	}
		
		function updateAppDesign($id,$data) {
	        $this->db->where('AppId', $id);
	        $update_data = $this->db->update('app_designs', $data);
	        if ($update_data == TRUE) {
	            return TRUE;
	        } else {
	            return FALSE;
	        }
    	}
		
		public function getAllRegIDForUserID($user_id) {
	    	$query = $this->db->query("SELECT gcm_reg_id FROM gcm_users WHERE user_id ='$user_id'");
			
			$outputarray = array();
			
			foreach ($query->result_array() as $row)
			{
				array_push($outputarray,$row['gcm_reg_id']);
			}
			
			return $outputarray;
	    }
		
		public function pushMessage($orgId,$message,$isVideo){
		
			$query = $this->db->query("CALL pushMessage('$orgId','$message',$isVideo)");
	        if ($query) {
	            return 1;
	        }
			else 
			{
	            return -1;
	        }
		}
		
		
    }
?>