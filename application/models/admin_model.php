<?php
    class Admin_Model extends CI_Model{
    	
		public function getAllRequest(){
			$query=$this->db->get('organization_requests');
			return $query;
		}
		
		public function deleteOrgRequest($org_id){
			$delete = $this->db->delete('organization_requests', array('OrganizationId' => $org_id));
	        if ($delete) {
	            return 1;
	        } else {
	            return -1;
	        }
		}
		
		function getApproveOrg($id){
	        $query = $this->db->get_where("organization_requests", array("OrganizationId" => $id));
	        if ($query->num_rows() > 0)
	        {
	           $result = $query->row(); 
	            return $result;
	        } else {
	            return FALSE;
	        }
    	}
		
		function insertUser($data) {
	        $query = $this->db->insert("users", $data);
	        if ($query) {
	            return TRUE;
	        } else {
	            return FALSE;
	        }
    }
    }
?>