<?php
	class Organization_Model extends CI_Model {
	   function __construct() {
	      // Call the Model constructor
	       parent::__construct();
		   $this->load->database();
	    }
	
	 
		 
		 function insertOrganization($data){
		 	//insert(tablename,data);
		 	$query = $this->db->insert('organization_requests', $data);
			
			 
	        if ($query) {
	            return 1;
	        } else {
	            return 0;
	        }
		 }
		 
		 function getMapPlaces($userId){
		 	$query = $this->db->query("SELECT * FROM distributed_branch WHERE OrgId ='".$userId."'");
			return $query;
			
		 }
		 
		 function addMapPlace($data) {
		 	
        	$query = $this->db->insert('distributed_branch', $data);
        	if ($query) {
            	return 1;
        	} else {
            	return 0;
        	}
    	}
		
		function addPromation($data){
			$query = $this->db->insert('branch_promotion', $data);
        	if ($query) {
            	return 1;
        	} else {
            	return 0;
        	}
		}
		
		function updatePromotion($id,$data) {
	        $this->db->where('PromId', $id);
	        $update_data = $this->db->update('branch_promotion', $data);
	        if ($update_data == TRUE) {
	            return TRUE;
	        } else {
	            return FALSE;
	        }
    	}
		
		
		
		function getPromotions($branchId){
			$query = $this->db->query("SELECT * FROM branch_promotion WHERE BranchId =".$branchId."");
			return $query;
		}
		
		function id_to_dbrow($table, $table_id_field, $id){
	        $query = $this->db->get_where($table, array($table_id_field => $id));
	        if ($query->num_rows() > 0)
	        {
	           $result = $query->row(); 
	            return $result;
	        } else {
	            return FALSE;
	        }
    	}
	
	}
?>
