<?php
	class App_Build_Model extends CI_Model {
	   function __construct() {
	      // Call the Model constructor
	       parent::__construct();
		   $this->load->database();
	    }
	
	 
		 function getAppName($OrgId){
		 	$query = $this->db->query("SELECT * FROM app_designs WHERE UserId = '$OrgId'");
			
			 
	        if ($query) {
				$row = $query->row();
	            return $row;
	        } else {
	            return 0;
	        }
		 }
		 
		 function getTextContent($userId){
		 	$query = $this->db->query("SELECT * FROM text_contents WHERE ClientId ='".$userId."'");
			return $query;
		 }
		 
		 function id_to_dbrow($table, $table_id_field, $id){
	        $query = $this->db->get_where($table, array($table_id_field => $id));
	        if ($query->num_rows() > 0)
	        {
	           $result = $query->row(); 
	            return $result;
	        } else {
	            return FALSE;
	        }
    	}
		
		function updateTextContent($id,$data) {
	        $this->db->where('ContentId', $id);
	        $update_data = $this->db->update('text_contents', $data);
	        if ($update_data == TRUE) {
	            return TRUE;
	        } else {
	            return FALSE;
	        }
    	}
		
		function deleteTextContent($id) {
			$delete = $this->db->delete('text_contents', array('ContentId' => $id));
			if ($delete) {
				return 1;
			} else {
				return -1;
			}
		}
		
		function addVideo($data){
			$query = $this->db->insert('video_contents', $data);
	        if ($query) {
	            return 1;
	        } else {
	            return 0;
	        }
		}
		
		function updatePlayVideoCurrentPOsition($videoId,$data) {
	        $this->db->where('VideoId', $videoId);
	        $update_data = $this->db->update('video_contents', $data);
	        if ($update_data == TRUE) {
	            return 1;
	        } else {
	            return -1;
	        }
    	}
		
		public function pushMessage($orgId,$message,$isVideo){
		
			$query = $this->db->query("CALL pushMessage('$orgId','$message',$isVideo)");
	        if ($query) {
	            return 1;
	        }
			else 
			{
	            return -1;
	        }
		}
		 
	
	}
?>
