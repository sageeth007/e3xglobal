<?php
class site_setting_model extends CI_Model {
	
	function __construct() {
      // Call the Model constructor
       parent::__construct();
    }
	
	function get_site_details(){
	 	
		$query=$this->db->query('SELECT * FROM site_setting');
		return $query;
		
	 }
         
         
	function id_to_dbrow($table, $table_id_field, $id){
        $query = $this->db->get_where($table, array($table_id_field => $id));
        if ($query->num_rows() > 0)
        {
           $result = $query->row(); 
            return $result;
        } else {
            return FALSE;
        }
    }
    
    function update($table_name, $table_field, $id, $data) {
        $this->db->where($table_field, $id);
        $update_data = $this->db->update($table_name, $data);
        if ($update_data == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function insert($table_name, $data) {
        $query = $this->db->insert($table_name, $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
  
    
	
}


?>