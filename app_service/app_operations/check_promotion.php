<?php
	require ('../db_control/db_access.php');
	if(!empty($_POST["org_id"]) && !empty($_POST["lat"]) && !empty($_POST["lng"])){
		$response = array();
		$orgId = $_POST["org_id"];
		$lat = $_POST["lat"];
		$lng = $_POST["lng"];
		
		$dba = new DB_Access();
		$result = $dba->getPromotionForLocation($orgId,$lat,$lng);
		
		if($result != -1){
			$response["promotions"] = $result;
			$response["state"] = 1;
		}
		else{
			$response = array("state" => -1);
			
		}
		echo json_encode($response);
	}
	
	else{


?>
	<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<div class="container">
		<h4>E3XGlobal -<strong>Get Promotions For Location</strong></h4>
		<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
			<br>
			<br>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Org ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="org_id" class="form-control" required>                                                   
               		</div>
            </div>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Lat : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="lat" class="form-control" required>                                                   
               		</div>
            </div>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Lon : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="lng" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
	</div>
	

<?php } ?>
