<?php 
	if(!empty($_POST["org_id"]) && !empty($_POST["title"]) && !empty($_POST["msg"])){
		require("../db_control/db_access.php");
		
		$title = $_POST["title"];
		$msg = $_POST["msg"];
		
		$dba = new DB_Access();
		$responce = array();
		$message = array("Message" => $msg , "MessageTitle" => $title);
		$d = json_encode($message);
		$responce["state"] = $dba->pushMessage(trim($_POST["org_id"]),$d);
		
		echo json_encode($responce);
		
	}

	else{
?>

<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<div class="container">
		<h4>E3XGloabal -<strong>Push Message</strong></h4>
		<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
			<br>
			<br>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Org ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="org_id" class="form-control" required>                                                   
               		</div>
            </div>
			
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Title : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="title" class="form-control" required>                                                   
               		</div>
            </div>
			
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Message : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="msg" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
	</div>

<?php }?>