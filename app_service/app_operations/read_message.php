<?php 
	if(!empty($_GET["device_id"])){
		require("../db_control/db_access.php");
		
		$dba = new DB_Access();
		$result = $dba->readMessage(trim($_GET["device_id"]));
		
		if($result != -1){
			echo $result;
		}
		
	}

	else{
?>

<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<div class="container">
		<h4>E3XGlobal -<strong>Read Message</strong></h4>
		<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get">
			<br>
			<br>
			
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Device ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="device_id" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
	</div>

<?php }?>