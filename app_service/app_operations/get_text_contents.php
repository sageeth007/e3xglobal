<?php
	require ('../db_control/db_access.php');
	if(!empty($_POST["user_id"])){

		$response["text_contents"] = array();
		$userId = $_POST["user_id"];
		
		$dba = new DB_Access();
		$result = $dba->getTextContents($userId);
		
		if($result != -1){
			$response["text_contents"] = $result;
			$response["state"] = 1;
		}
		else{
			$response["state"] = -1;
			
		}
		echo json_encode($response);
	}

	else{

?>

	<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<div class="container">
		<h4>E3XGlobal -<strong>Get Text Contents</strong></h4>
		<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
			<br>
			<br>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">User ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="user_id" class="form-control" required>                                                   
               		</div>
            </div>
			
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
	</div>



<?php }?>