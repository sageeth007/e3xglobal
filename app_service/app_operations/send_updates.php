<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
<div class="container">
		<h4>E3XGlobal -<strong>Send Update Notification Demo</strong></h4>
		<form class="form-horizontal" action="../gcm/send_message.php" method="post">
			<br>
			<br>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Client User ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="user_id" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Message : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="message" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
</div>