<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<title>Video Stream App View</title>
		<script type="text/javascript" src="../../../assets/site/js/jquery.min.js"></script>
		<script type="text/javascript" src="../../../assets/site/js/jquery.js"></script>
		<script type="text/javascript" >
			var player;
			var videoId = -1;
			var videoUrl;
			var currentPosition;
			var interval;
			
			function init(){
				player = document.getElementById('player');
				interval = setInterval(function(){getStream()},1000);
			}
			
			function loadVideo(){
				
				document.getElementById('video_src').setAttribute('src',videoUrl);
				player.load();
			}
			function seek(){
				try{
					player.currentTime = currentPosition;
				}
				catch(err){
				}

			
			}
			
			function getStream(){
			
				var orgId = 'Sirasa';
				$.ajax({
							'url' : 'check_stream_video.php',
							'type' : 'POST', 
							'data' : {	'org_id' : orgId
									  },
							success: function(data) {
								
								var rsult = JSON.parse(data)
								if(rsult.state == 1){
									videoUrl = rsult.url;
									currentPosition = rsult.currentPlayPosition;
									if(rsult.videoId != videoId){
										videoId = rsult.videoId;
										loadVideo();
									}
									else{
										seek();
										player.play();
									}
								}
								else{
									getPausePosition();
								}
									
							},
				});
			}
			
			function getPausePosition(){
				
				$.ajax({
							'url' : 'get_pause_position.php',
							'type' : 'POST', 
							'data' : {	'video_id' : videoId
									  },
							success: function(data) {
									
									var rsult = JSON.parse(data)
									if(rsult.state == 1){
										currentPosition = rsult.currentPlayPosition;
										player.pause();
										seek();
										
									}
							},
				});
			}
		
		
		</script>
		
		
		<style>
		video {
			max-width: 100%;
			height: auto;
		}

		</style>


	</head>

	<body onload="init()">

		<div id="pagewrap">

			<video id="player" width="700" height="390" controls="control"  autoplay> 
				<source id="video_src" src='' type="video/mp4">
			</video>
			
		</div>

	</body>
</html>