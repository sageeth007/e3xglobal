<?php if(!empty($_GET['org_id'])){ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>E3xGlobal</title>
	<script type="text/javascript" src="../../iscroll-master/build/iscroll.js"></script>
	<script type="text/javascript" src="../../../assets/site/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../../assets/site/js/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="e3xglobal_css.css">
	<script type="text/javascript">

		var myScroll;
		var scrollContent;
		function loaded () {
			
			myScroll = new IScroll('#wrapper', { mouseWheel: true });
			scrollContent = document.getElementById('scroller');
			loadContent();
			setInterval(function(){loadContent()},1000);
		}
		
		function loadContent(){
			
			var orgId = '<?php echo $_GET['org_id']?>';
			$.ajax({
						'url' : 'get_text_contents.php',
						'type' : 'POST', 
						'data' : {	'org_id' : orgId
								  },
						success: function(data) {
								scroller.innerHTML = data;
								myScroll.refresh();
								
						},
			});
		}

		document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);

	</script>
	
	
	
	
</head>
<body onload="loaded()">
<div id="wrapper">
	<div id="scroller">
		<ul>
			<?php for($i=0;$i<3;$i++){?>
				<li align="center">
				
					<div id="cotent_tab">
						<div id="cotent_tab_headder" align="left">
							<div id="cotent_tab_headder_title" ></div>
							<div id="cotent_tab_headder_date" align="right"></div>
							</br>
							<p id="cotent_tab_headder_p">
								
								
								
							</p>
						</div>
						
					</div>
					
				
				</li>
			
			<?php }?>
		</ul>
	</div>
</div>

</body>
</html>

<?php }else{ ?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="e3xglobal_css.css">
	<div id="progress">Error Org Id</div>
<?php }?>