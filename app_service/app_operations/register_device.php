<?php
	require ('../db_control/db_access.php');
	if(!empty($_POST["org_id"]) && !empty($_POST["device_id"])){
		$response = array();
		$userId = $_POST["org_id"];
		$deviceId = $_POST["device_id"];
		
		$dba = new DB_Access();
		$result = $dba->registerDevice($userId,$deviceId);
		
		if($result == 1){
			$response["state"] = 1;
		}
		else{
			$response["state"] = -1;
			
		}
		echo json_encode($response);
	}
	
	else{


?>
	<link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<div class="container">
		<h4>E3XGlobal -<strong>Register In Message</strong></h4>
		<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
			<br>
			<br>
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Client User ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="org_id" class="form-control" required>                                                   
               		</div>
            </div>
			
			<div class="form-group">
                    <label for="textfield" class="col-md-4 control-label">Device ID : </label>
                    <div class="input-group col-md-4"> 
                    	<input type="text"  name="device_id" class="form-control" required>                                                   
               		</div>
            </div>
            
            <div class="form-group" id="button" align="center" >
					<button class="btn btn-primary "  type="submit">Submit</button>
                                         
					<button type="button"   type="button" class="btn btn-clear"  
					onclick="window.location=''">Cancle</button>
            </div>
			
		</form>
	</div>
	

<?php } ?>
