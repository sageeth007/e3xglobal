<?php
	require ('db_connect.php');
	class DB_Access{
		
		function __construct(){
			
			DB_Connect::getConnection();
			
		}
		function closeConnection(){
			DB_Connect::cloceConnection();
		}
		
		function getAppDesign($userId)
		{
			try{
				
				//get all app design details according to user is 
				$result = mysql_query("SELECT * FROM app_designs WHERE UserId = '".$userId."'");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) == 1)
				{
					$outputarray = array();
					
					while($row = mysql_fetch_row($result))
					{
						$outputarray["AppName"] = $row[1];
						$outputarray["AppDescription"] = $row[2];
						$outputarray["ActionBarColor"] = $row[3];
						$outputarray["BodyColor"] = $row[4];
						$outputarray["AppLogo"] = $row[5];
						$outputarray["ContentsAds_V"] = $row[6];
						
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{ return -1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
		
		
		
	    public function storeGCMUser($user_id, $gcm_regid) {
	        try{	
		        // insert user into database
		        $result = mysql_query("INSERT INTO gcm_users(user_id, gcm_reg_id) VALUES('$user_id', '$gcm_regid')");
		        
				if($result == FALSE){throw new Exception();}
				
		        // check for successful store
		        if ($result) {
		            
		            $id = mysql_insert_id(); 
		            $result = mysql_query("SELECT * FROM gcm_users WHERE id = $id");
		            
					if($result == FALSE){throw new Exception();}
					
		            if (mysql_num_rows($result) > 0) {
		                return 1;
		            } 
		            else {
		                return -1;
		            }
		        } 
		        else {
		            return -1;
		        }
			}
			catch(Exception $e){
				return -1;
			}
	    }
	
	    public function getAllRegIDForUserID($user_id) {
	    	try{
	        	$result = mysql_query("select * FROM gcm_users WHERE user_id = '$user_id'");
	        	if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) > 0)
				{
					$outputarray = array();
					$i = 0;
					while($row = mysql_fetch_row($result))
					{
						array_push($outputarray,$row[2]);
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{ return -1;}
				
				
				
				
				
			}
			catch(Exception $e){
				return -1;
			}
	    }
	
	    function getTextContents($userId)
		{
			try{
				
				//get all app design details according to user is 
				$result = mysql_query("SELECT * FROM text_contents WHERE ClientId = '".$userId."'");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) > 0)
				{
					$outputarray = array();
					$i = 0;
					while($row = mysql_fetch_row($result))
					{
						$outputarray[$i]["ContentId"] = $row[0];
						$outputarray[$i]["ContentTitle"] = $row[1];
						$outputarray[$i]["ContentBody"] = $row[2];
						$outputarray[$i]["ContentLable"] = $row[3];
						$outputarray[$i]["Date"] = $row[4];
						$i++;
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{ return -1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
		
		function getPromotionForLocation($orgId,$lat,$lon)
		{
			try{
				
				//get all app design details according to user is 
				$result = mysql_query("CALL checkPromotiosForLocation('$orgId',$lat,$lon)");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) > 0)
				{
					$outputarray = array();
					$i = 0;
					while($row = mysql_fetch_row($result))
					{
						$outputarray[$i]["PromId"] = $row[0];
						$outputarray[$i]["Title"] = $row[1];
						$outputarray[$i]["Content"] = $row[2];
						$outputarray[$i]["ImagePath"] = $row[3];
						$i++;
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{ return -1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
		
		function registerDevice($orgId,$deviceId)
		{
			try{
				
				$result = mysql_query("INSERT INTO reg_devices VALUES('$orgId','$deviceId')");
				
				if($result == FALSE){throw new Exception();}
				
				else{ return 1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
		
		function pushMessage($orgId,$message)
		{
			try{
				
				$result = mysql_query("CALL pushMessage('$orgId','$message',0)");
				
				if($result == FALSE){throw new Exception();}
				
				else
				{
					mysql_free_result($result);
					return 1;
				}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
		
		function readMessage($deviceId)
		{
			try{
				
				$result = mysql_query("CALL readMessage('$deviceId')");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) == 1)
				{
					$outputarray = "";
					if($row = mysql_fetch_row($result))
					{
						if($row[0] != -1){
							$outputarray = $row[1];
						}
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{
					mysql_free_result($result);
					return -1;
				}
					
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
			
		}
	    
		function checkStreamVideo($orgId){
			try{
				
				$result = mysql_query("SELECT VideoId,CurrentPlayPosition FROM video_contents WHERE ClientId = '$orgId' AND NowPlaying = 1 LIMIT 0,1;");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) == 1)
				{
					$outputarray = array();
					
					if($row = mysql_fetch_row($result))
					{
						$outputarray["videoId"] = $row[0];
						$outputarray["currentPlayPosition"] = $row[1];
						$video = $this->loadVideo($row[0]);
						$outputarray["name"] = $video["name"];
						$outputarray["url"] = $video["FLVURL"];
					}
					mysql_free_result($result);
					return $outputarray;
				}
				else{ return -1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
		}
		
		private function loadVideo($id){
		
			$fields = "name,FLVURL";
			$readToken = "CdKLZMLbLpeOUeSduDaLWrn6eIL9n6o_EiM-wO6UCjhY9urh9BFJEQ..";
			$url = "http://api.brightcove.com/services/library?command=find_video_by_id&video_id=".$id."&video_fields=".$fields."&media_delivery=http&token=".$readToken;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_VERBOSE, TRUE );
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
			curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		
			$result = curl_exec($curl);
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($curl));
				return array(
						'state'=>-1,
						'message'=>'Error Loading videos');
			}		
			else{
				$output = json_decode($result,true);
				$output["state"] = 1;
				return $output;	
			}
				
			curl_close($curl);
	
		}
		
		public function getPausePosition($videoId){
			
			try{
				
				$result = mysql_query("SELECT CurrentPlayPosition FROM video_contents WHERE VideoId = '$videoId'");
				
				if($result == FALSE){throw new Exception();}
				
				if(mysql_num_rows($result) == 1)
				{
					$output= "";
					
					if($row = mysql_fetch_row($result))
					{
						$output = $row[0];
					}
					mysql_free_result($result);
					return $output;
				}
				else{ return -1;}
			}
			
			catch(Exception $e)
			{ 
				return -1;
			}
		}
		
	}
	



?>