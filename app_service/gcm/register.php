<?php

$json = array();
include("../db_control/db_access.php");
include("gcm.php");

if (isset($_POST["user_id"]) && isset($_POST["regId"])) {
    	
    $userId = $_POST["user_id"];
    $gcmRegid = $_POST["regId"]; 
   
    $dba = new DB_Access();
    $gcm = new GCM();

    $res = $dba->storeGCMUser($userId,$gcmRegid);

    $registatoin_ids = array($gcmRegid);
    $message = array("MessageTitle" => "register");

    $result = $gcm->send_notification($registatoin_ids, $message);

    echo $result;
} else {
    // user details missing
}
?>